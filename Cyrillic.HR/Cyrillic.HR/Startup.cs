﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cyrillic.HR.Startup))]
namespace Cyrillic.HR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
