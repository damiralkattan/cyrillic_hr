﻿using Cyrillic.HR.Models;
using Cyrillic.HR.Models.TalentResourceManager;
using Cyrillic.HR.ViewModels.TalentResourceManager;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class OrganizationController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Organization
        public ActionResult Index()
        {
            var organizationsIndexViewModel = new OrganizationIndexViewModel
            {
                CanManipulate = User.IsInRole("SysAdmin")
            };

            var userId = User.Identity.GetUserId();
            organizationsIndexViewModel.Organizations = GetOrganizations();
            
            return View(organizationsIndexViewModel);
        }

        // GET: Organization/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organization = db.Organizations.Find(id);
            var allowedOrgs = GetOrganizations();
            if(allowedOrgs.Contains(organization))
            {
                return View(organization);
            }

            if (organization == null)
            {
                return HttpNotFound();
            }

            return RedirectToAction("Index");
        }

        // GET: Organization/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Organization/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,EmailHostname,EmailAddress,EmailPassword,EmailInboxes")] Organization organization)
        {
            if (ModelState.IsValid)
            {
                db.Organizations.Add(organization);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(organization);
        }

        // GET: Organization/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organization = db.Organizations.Find(id);
            var allowedOrgs = GetOrganizations();
            if (allowedOrgs.Contains(organization))
            {
                return View(organization);
            }
                if (organization == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index");
        }

        // POST: Organization/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,EmailHostname,EmailAddress,EmailPassword,EmailInboxes")] Organization organization)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organization).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(organization);
        }

        // GET: Organization/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organization = db.Organizations.Find(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }

        // POST: Organization/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var organization = db.Organizations.Find(id);
            db.Organizations.Remove(organization);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private List<Organization> GetOrganizations()
        {
            if (User.IsInRole("SysAdmin"))
            {
                return db.Organizations.ToList();
            }
            var list = new List<Organization>();
            var userId = User.Identity.GetUserId();
            return db.Users.Include(o => o.Organizations).Single(u => u.Id == userId).Organizations.ToList();
        }
    }
}
