﻿using System.Web.Mvc;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class TalentResourceManagerHandlerController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
