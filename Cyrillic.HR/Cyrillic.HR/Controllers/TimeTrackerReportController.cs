﻿using Cyrillic.HR.Models;
using Cyrillic.HR.ViewModels.TimeTracker;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class TimeTrackerReportController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = new ReportViewModel
            {
                Months = new Dictionary<string, string>
                {
                    { "1", "January" },
                    { "2", "February" },
                    { "3", "March" },
                    { "4", "April" },
                    { "5", "May" },
                    { "6", "June" },
                    { "7", "July" },
                    { "8", "August" },
                    { "9", "September" },
                    { "10", "October" },
                    { "11", "November" },
                    { "12", "December" }
                },
                MonthCustomChoice = "month"
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(ReportViewModel viewModel)
        {
            #region Initialize Months for Months Dropdown List

            viewModel.Months = new Dictionary<string, string>
            {
                { "1", "January" },
                { "2", "February" },
                { "3", "March" },
                { "4", "April" },
                { "5", "May" },
                { "6", "June" },
                { "7", "July" },
                { "8", "August" },
                { "9", "September" },
                { "10", "October" },
                { "11", "November" },
                { "12", "December" }
            };

            #endregion Initialize Months for Months Dropdown List

            DateTime intervalStart;
            DateTime intervalEnd;
            switch (viewModel.MonthCustomChoice)
            {
                case "month":
                    {
                        var monthOrdinalNumber = viewModel.SelectedMonthOrdinalNumber;
                        intervalStart = new DateTime(DateTime.Now.Year, monthOrdinalNumber, 1);
                        intervalEnd = new DateTime(DateTime.Now.Year, monthOrdinalNumber, intervalStart.AddMonths(1).AddDays(-1).Day);
                        break;
                    }
                case "custom":
                    {
                        intervalStart = DateTime.Parse(viewModel.Start);
                        intervalEnd = DateTime.Parse(viewModel.End);
                        break;
                    }
                default:
                    {
                        ViewBag.Message = "Could not resolve timeframe";
                        return View(viewModel);
                    }
            }

            var userId = User.Identity.GetUserId();

            #region Get Report by Project

            viewModel.Projects = new List<ReportProjectViewModel>();
            var projects = _db.Projects.ToList();
            foreach (var project in projects)
            {
                var timeEntriesForProjectInTimeframe = _db.TimeEntries
                    .Where(te => te.ProjectId == project.Id && (intervalStart <= te.Date && te.Date <= intervalEnd));

                if (User.IsInRole("OrgAdmin"))
                {
                    var orgsOfCurrentOrgAdminUser = _db.Users
                        .Include(o => o.Organizations)
                        .Single(u => u.Id == userId)
                        .Organizations
                        .ToList();

                    timeEntriesForProjectInTimeframe = orgsOfCurrentOrgAdminUser
                        .Select
                        (
                            organization => _db.Organizations
                                .Include(o => o.ApplicationUsers)
                                .Single(o => o.Id == organization.Id)
                                .ApplicationUsers
                                .Select(user => user.Id)
                                .ToList()
                        )
                        .Aggregate
                        (
                            timeEntriesForProjectInTimeframe, (current, userIds) =>
                            (
                                from te
                                in current
                                where userIds.Contains(te.ApplicationUserId)
                                select te
                            )
                        );
                }
                else if (User.IsInRole("OrgUser"))
                {
                    timeEntriesForProjectInTimeframe = timeEntriesForProjectInTimeframe.Where(te => te.ApplicationUserId == userId);
                }

                if (timeEntriesForProjectInTimeframe.Any())
                {
                    viewModel.Projects.Add(new ReportProjectViewModel
                    {
                        Name = project.Name,
                        TotalHours = timeEntriesForProjectInTimeframe.Sum(te => te.Hours)
                    });
                }
            }

            #endregion

            #region Get Report by User

            viewModel.Users = new List<ReportUserViewModel>();
            var users = new List<ApplicationUser>();

            if (User.IsInRole("SysAdmin"))
            {
                users = _db.Users.ToList();
            }
            else if (User.IsInRole("OrgAdmin"))
            {
                var orgsOfCurrentUser = _db.Users.Include(o => o.Organizations).Single(u => u.Id == userId).Organizations.ToList();

                foreach (var organization in orgsOfCurrentUser)
                {
                    users.AddRange(_db.Organizations.Include(o => o.ApplicationUsers).Single(o => o.Id == organization.Id).ApplicationUsers.ToList());
                }
            }
            else
            {
                users = _db.Users.Where(u => u.Id == userId).ToList();
            }

            foreach (var user in users)
            {
                var timeEntriesForUserInTimeframe = _db.TimeEntries
                    .Where(te => te.ApplicationUserId == user.Id && (intervalStart <= te.Date && te.Date <= intervalEnd));

                if (!timeEntriesForUserInTimeframe.Any()) continue;
                {
                    var reportUserViewModel = new ReportUserViewModel { UserName = user.UserName };

                    var timeEntriesForUserInTimeframeRegular = timeEntriesForUserInTimeframe.Where(te => !te.Overtime);
                    if (timeEntriesForUserInTimeframeRegular.Any())
                    {
                        reportUserViewModel.TotalHoursRegular = timeEntriesForUserInTimeframeRegular.Sum(te => te.Hours);
                    }

                    var timeEntriesForUserInTimeframeOvertime = timeEntriesForUserInTimeframe.Where(te => te.Overtime);
                    if (timeEntriesForUserInTimeframeOvertime.Any())
                    {
                        reportUserViewModel.TotalHoursOvertime = timeEntriesForUserInTimeframeOvertime.Sum(te => te.Hours);
                    }

                    viewModel.Users.Add(reportUserViewModel);
                }
            }

            #endregion

            return View(viewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
