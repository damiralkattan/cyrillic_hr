﻿using System.Linq;
using System.Web.Mvc;
using Cyrillic.HR.Models;
using Cyrillic.HR.ViewModels.TimeTracker;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class TimeTrackerHandlerController : Controller
    {
        public ActionResult Index()
        {
            int numberOfClients;
            int numberOfProjects;
            int numberOfProjectTasks;
            using (var db = new ApplicationDbContext())
            {
                numberOfClients = db.Clients.Count();
                numberOfProjects = db.Projects.Count();
                numberOfProjectTasks = db.ProjectTasks.Count();
            }

            var viewModel = new ManagementViewModel
            {
                NumberOfClients = numberOfClients,
                NumberOfProjects = numberOfProjects,
                NumberOfProjectTasks = numberOfProjectTasks
            };

            return View(viewModel);
        }
    }
}
