﻿using Cyrillic.HR.Models;
using Cyrillic.HR.ViewModels.TimeTracker;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Cyrillic.HR.Models.TimeTracker;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class TimeTrackerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private static IEnumerable<DateTime> EachDay(DateTime? start, DateTime? end)
        {
            for (var day = start?.Date; day?.Date <= end?.Date; day = day.Value.AddDays(1))
                yield return (DateTime)day;
        }

        private static DateTime GetStartDateFromCurrentWeek()
        {
            var today = DateTime.Today;
            DateTime start;
            switch (today.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    {
                        start = today;
                    }
                    break;
                case DayOfWeek.Tuesday:
                    {
                        start = today.AddDays(-1);
                    }
                    break;
                case DayOfWeek.Wednesday:
                    {
                        start = today.AddDays(-2);
                    }
                    break;
                case DayOfWeek.Thursday:
                    {
                        start = today.AddDays(-3);
                    }
                    break;
                case DayOfWeek.Friday:
                    {
                        start = today.AddDays(-4);
                    }
                    break;
                case DayOfWeek.Saturday:
                    {
                        start = today.AddDays(-5);
                    }
                    break;
                case DayOfWeek.Sunday:
                    {
                        start = today.AddDays(-6);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return start;
        }

        private static DateTime GetEndDateFromCurrentWeek()
        {
            var today = DateTime.Today;
            DateTime end;
            switch (today.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    {
                        end = today.AddDays(6);
                    }
                    break;
                case DayOfWeek.Tuesday:
                    {
                        end = today.AddDays(5);
                    }
                    break;
                case DayOfWeek.Wednesday:
                    {
                        end = today.AddDays(4);
                    }
                    break;
                case DayOfWeek.Thursday:
                    {
                        end = today.AddDays(3);
                    }
                    break;
                case DayOfWeek.Friday:
                    {
                        end = today.AddDays(2);
                    }
                    break;
                case DayOfWeek.Saturday:
                    {
                        end = today.AddDays(1);
                    }
                    break;
                case DayOfWeek.Sunday:
                    {
                        end = today;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return end;
        }

        public ActionResult AdvanceTimeSheet(DateTime currentstartdate)
        {
            var newStartDate = currentstartdate.AddDays(7);
            var newEndDate = newStartDate.AddDays(6);

            return RedirectToAction("Index", new { newStartDate, newEndDate });
        }

        public ActionResult RegressTimeSheet(DateTime currentstartdate)
        {
            var newStartDate = currentstartdate.AddDays(-7);
            var newEndDate = newStartDate.AddDays(6);

            return RedirectToAction("Index", new { newStartDate, newEndDate });
        }

        public ActionResult Index(DateTime? newStartDate = null, DateTime? newEndDate = null)
        {
            #region If Start And End Days Are Null Use Current Week

            if (newStartDate == null && newEndDate == null)
            {
                newStartDate = GetStartDateFromCurrentWeek();
                newEndDate = GetEndDateFromCurrentWeek();
            }

            #endregion

            var applicationUserId = User.Identity.GetUserId();

            var timeSheetViewModel = new TimeSheetViewModel
            {
                Db = db,
                ApplicationUserId = applicationUserId,
                TimeSheetDays = new List<TimeSheetDayViewModel>()
            };

            foreach (var day in EachDay(newStartDate, newEndDate))
            {
                timeSheetViewModel.TimeSheetDays.Add(new TimeSheetDayViewModel
                {
                    Date = day,
                    Name = day.ToString("dddd"),
                    TimeSheetTimeEntries = db.TimeEntries.Where(
                        te => te.ApplicationUserId == applicationUserId && te.Date == day)
                        .ToList()
                        .Select(te => new TimeSheetTimeEntryViewModel(db)
                        {
                            WeekStartDate = newStartDate.GetValueOrDefault(),
                            WeekEndDate = newEndDate.GetValueOrDefault(),
                            Id = te.Id,
                            ApplicationUserId = te.ApplicationUserId,
                            Date = te.Date,
                            ClientId = te.ClientId,
                            ProjectId = te.ProjectId,
                            ProjectTaskId = te.ProjectTaskId,
                            Comment = te.Comment,
                            Hours = te.Hours,
                            Overtime = te.Overtime
                        })
                        .ToList()
                });
            }

            timeSheetViewModel.StartDay = timeSheetViewModel.TimeSheetDays.First().Date;
            timeSheetViewModel.EndDay = timeSheetViewModel.TimeSheetDays.Last().Date;
            timeSheetViewModel.StartDayFormatted = timeSheetViewModel.TimeSheetDays.First().Date.ToString("dd MMMM yyyy");
            timeSheetViewModel.EndDayFormatted = timeSheetViewModel.TimeSheetDays.Last().Date.ToString("dd MMMM yyyy");

            return View(timeSheetViewModel);
        }

        [HttpPost]
        public ActionResult ManageTimeEntry(TimeSheetTimeEntryViewModel timeSheetTimeEntryViewModel)
        {
            if (timeSheetTimeEntryViewModel.Id == 0)
            {
                //Adding new entry to the database
                db.TimeEntries.Add(new TimeEntry
                {
                    ApplicationUserId = timeSheetTimeEntryViewModel.ApplicationUserId,
                    Date = timeSheetTimeEntryViewModel.Date,
                    ClientId = timeSheetTimeEntryViewModel.ClientId,
                    ProjectId = timeSheetTimeEntryViewModel.ProjectId,
                    ProjectTaskId = timeSheetTimeEntryViewModel.ProjectTaskId,
                    Comment = timeSheetTimeEntryViewModel.Comment,
                    Hours = timeSheetTimeEntryViewModel.Hours,
                    Overtime = timeSheetTimeEntryViewModel.Overtime
                });
            }
            else
            {
                //Editing an existing entry in the database
                var existingTimeEntry = db.TimeEntries.Single(te => te.Id == timeSheetTimeEntryViewModel.Id);

                existingTimeEntry.ClientId = timeSheetTimeEntryViewModel.ClientId;
                existingTimeEntry.ProjectId = timeSheetTimeEntryViewModel.ProjectId;
                existingTimeEntry.ProjectTaskId = timeSheetTimeEntryViewModel.ProjectTaskId;
                existingTimeEntry.Comment = timeSheetTimeEntryViewModel.Comment;
                existingTimeEntry.Hours = timeSheetTimeEntryViewModel.Hours;
                existingTimeEntry.Overtime = timeSheetTimeEntryViewModel.Overtime;
            }

            db.SaveChanges();

            return RedirectToAction("Index", new { newStartDate = timeSheetTimeEntryViewModel.WeekStartDate, newEndDate = timeSheetTimeEntryViewModel.WeekEndDate });
        }
    }
}
