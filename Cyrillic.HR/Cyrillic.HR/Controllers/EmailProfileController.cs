﻿using Cyrillic.HR.Models;
using Cyrillic.HR.Models.TalentResourceManager;
using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using Cyrillic.HR.ViewModels.TalentResourceManager;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class EmailProfileController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EmailProfile
        public ActionResult Index()
        {
            var viewModel = new ShowEmailProfilesViewModel();

            if(User.IsInRole("OrgUser"))
            {
                viewModel.UserCanEdit = false;
            }
            else
            {
                viewModel.UserCanEdit = true;
            }

            var profiles = GetEmailProfilesForOrganizations();
            viewModel.EmailProfiles = new List<EmailProfile>();
            foreach (var profile in profiles)
            {
                profile.Organization = db.Organizations.FirstOrDefault(x => x.Id == profile.OrganizationId);
                viewModel.EmailProfiles.Add(profile);
            }
            return View(viewModel);
        }

        // GET: EmailProfile/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var emailProfile = db.EmailProfiles.Find(id);
            if (emailProfile == null)
            {
                return HttpNotFound();
            }
            return View(emailProfile);
        }

        // GET: EmailProfile/Create
        public ActionResult Create()
        {
            ViewBag.OrganizationId = new SelectList(GetOrganizations(), "Id", "Name");
            return View();
        }

        // POST: EmailProfile/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Hostname,Port,Username,Password,InboxName,OrganizationId")] EmailProfile emailProfile)
        {
            if (ModelState.IsValid)
            {
                emailProfile.Organization = db.Organizations.FirstOrDefault(x => x.Id == emailProfile.OrganizationId);
                db.EmailProfiles.Add(emailProfile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.OrganizationId = new SelectList(GetOrganizations(), "Id", "Name", emailProfile.OrganizationId);
            return View(emailProfile);
        }

        // GET: EmailProfile/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var emailProfile = db.EmailProfiles.Find(id);
            if (emailProfile == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganizationId = new SelectList(GetOrganizations(), "Id", "Name", emailProfile.OrganizationId);
            return View(emailProfile);
        }

        // POST: EmailProfile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Hostname,Port,Username,Password,InboxName,OrganizationId")] EmailProfile emailProfile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emailProfile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganizationId = new SelectList(GetOrganizations(), "Id", "Name", emailProfile.OrganizationId);
            return View(emailProfile);
        }

        // GET: EmailProfile/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var emailProfile = db.EmailProfiles.Find(id);
            if (emailProfile == null)
            {
                return HttpNotFound();
            }
            return View(emailProfile);
        }

        // POST: EmailProfile/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var emailProfile = db.EmailProfiles.Find(id);
            db.EmailProfiles.Remove(emailProfile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private List<Organization> GetOrganizations()
        {
            if (User.IsInRole("SysAdmin"))
            {
                return db.Organizations.ToList();
            }
            var list = new List<Organization>();
            var userId = User.Identity.GetUserId();
            return db.Users.Include(o => o.Organizations).Single(u => u.Id == userId).Organizations.ToList();
        }

        private List<EmailProfile> GetEmailProfilesForOrganizations()
        {
            if (User.IsInRole("SysAdmin"))
            {
                return db.EmailProfiles.ToList();
            }
            var list = new List<EmailProfile>();
            var userId = User.Identity.GetUserId();

            foreach (var org in GetOrganizations())
            {
                list.AddRange(db.Organizations.Include(x => x.EmailProfiles).Single(ep => ep.Id == org.Id).EmailProfiles);
            }
            return list;
        }
    }
}
