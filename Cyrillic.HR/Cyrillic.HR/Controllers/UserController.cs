﻿using Cyrillic.HR.Models;
using Cyrillic.HR.Models.TalentResourceManager;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public UserController()
        {
        }

        public UserController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        private ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        private ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            set
            {
                _roleManager = value;
            }
        }

        //
        // GET: /Users/
        public async Task<ActionResult> Index()
        {
            if (User.IsInRole("SysAdmin"))
            {
                return View(await UserManager.Users.ToListAsync());
            }

            List<Organization> orgs;
            if (User.IsInRole("SysAdmin"))
            {
                orgs = _db.Organizations.ToList();
            }
            else
            {
                var userId = User.Identity.GetUserId();
                orgs = _db.Users.Include(o => o.Organizations).Single(u => u.Id == userId).Organizations.ToList();
            }

            var users = new List<ApplicationUser>();
            foreach (var org in orgs)
            {
                users.AddRange(_db.Organizations.Include(us => us.ApplicationUsers).Single(x => x.Id == org.Id).ApplicationUsers.ToList());
            }

            return View(users);
        }

        //
        // GET: /Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);

            ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);

            var userId = User.Identity.GetUserId();
            using (var db = ApplicationDbContext.Create())
            {
                ViewBag.Organizations = db.Users.Include(u => u.Organizations).Single(u => u.Id == userId).Organizations.ToList();
            }

            return View(user);
        }

        //
        // GET: /Users/Create
        [Authorize(Roles = "SysAdmin, OrgAdmin")]
        public async Task<ActionResult> Create()
        {
            //Get the list of Roles
            ViewBag.RoleId =
                User.IsInRole("SysAdmin") ?
                new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name")
                :
                new SelectList(await RoleManager.Roles.Where(r => r.Name != "SysAdmin" && r.Name != "OrgAdmin").ToListAsync(), "Name", "Name");

            return View();
        }

        //
        // POST: /Users/Create
        [Authorize(Roles = "SysAdmin, OrgAdmin")]
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = userViewModel.UserName,
                    Email = userViewModel.Email,
                    EmailConfirmed = true,
                    LockoutEnabled = false
                };
                var adminresult = await UserManager.CreateAsync(user, userViewModel.Password);

                //Add User to the selected Roles 
                if (adminresult.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        //var result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                        var result = await UserManager.AddToRoleAsync(user.Id, selectedRoles[0]);
                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", result.Errors.First());
                            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                            return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", adminresult.Errors.First());
                    ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                    return View();

                }
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
            return View();
        }

        //
        // GET: /Users/Edit/1
        [Authorize(Roles = "SysAdmin, OrgAdmin")]
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userRoles = await UserManager.GetRolesAsync(user.Id);

            var viewModel = new EditUserViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                RolesList = User.IsInRole("SysAdmin") ?
                    RoleManager.Roles.ToList().Select(x => new SelectListItem
                    {
                        Selected = userRoles.Contains(x.Name),
                        Text = x.Name,
                        Value = x.Name
                    })
                    :
                    RoleManager.Roles.Where(r => r.Name != "SysAdmin" && r.Name != "OrgAdmin").ToList().Select(x => new SelectListItem
                    {
                        Selected = userRoles.Contains(x.Name),
                        Text = x.Name,
                        Value = x.Name
                    }),
            };

            return View(viewModel);
        }

        //
        // POST: /Users/Edit/5
        [Authorize(Roles = "SysAdmin, OrgAdmin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserName,Email,Id")] EditUserViewModel editUser, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(editUser.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                user.UserName = editUser.UserName;
                user.Email = editUser.Email;

                var userRole = await UserManager.GetRolesAsync(user.Id);

                selectedRole = selectedRole ?? new string[] { };

                var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRole).ToArray());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }

                result = await UserManager.RemoveFromRolesAsync(user.Id, userRole.Except(selectedRole).ToArray());
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError("", result.Errors.First());
                return View();
            }
            ModelState.AddModelError("", "Something failed.");
            return View();
        }

        //
        // GET: /Users/Delete/5
        [Authorize(Roles = "SysAdmin, OrgAdmin")]
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Delete/5
        [Authorize(Roles = "SysAdmin, OrgAdmin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                var rolesForUser = await UserManager.GetRolesAsync(id);
                if (rolesForUser.Count > 0)
                {
                    foreach (var item in rolesForUser.ToList())
                    {
                        // item should be the name of the role
                        var removeFromRoleResult = await UserManager.RemoveFromRoleAsync(user.Id, item);
                        if (removeFromRoleResult.Succeeded) continue;
                        ModelState.AddModelError("", removeFromRoleResult.Errors.First());
                        return View();
                    }
                }

                var result = await UserManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError("", result.Errors.First());
                return View();
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}