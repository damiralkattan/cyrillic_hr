﻿using Cyrillic.HR.Models;
using Cyrillic.HR.Models.TalentResourceManager;
using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using Cyrillic.HR.ViewModels;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class ParsingRuleController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ParsingRule
        public ActionResult Index()
        {
            var viewModel = new ShowParsingRulesViewModel();
            viewModel.ParsingRules = GetParsingRulesForEmailProfile();
            if (User.IsInRole("OrgUser"))
            {
                viewModel.UserCanEdit = false;
            }
            else
            {
                viewModel.UserCanEdit = true;
            }
            return View(viewModel);
        }

        // GET: ParsingRule/Details/5
        public ActionResult Details(int? id)
        {
            var viewModel = new ParsingRuleViewModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var parsingRule = db.ParsingRules.Find(id);
            if (parsingRule == null)
            {
                return HttpNotFound();
            }
            viewModel.ParsingRule = parsingRule;

            if (User.IsInRole("OrgUser"))
            {
                viewModel.UserCanEdit = false;
            }
            else
            {
                viewModel.UserCanEdit = true;
            }
            return View(viewModel);
        }

        // GET: ParsingRule/Create/5
        public ActionResult Create()
        {
            var list = new List<string>()
            {
                "Email",
                "FullName",
                "JobApplied"
            };
            ViewBag.CandidateProperties = new SelectList(list);
            ViewBag.EmailProfileId = new SelectList(GetEmailProfilesForOrganizations(), "Id", "Name");
            return View();
        }

        // POST: ParsingRule/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,EmailField,Regex,IsQualificationRule,CandidateProperty,EmailProfileId")] ParsingRule parsingRule)
        {
            if (ModelState.IsValid)
            {
                db.ParsingRules.Add(parsingRule);
                db.SaveChanges();
                return RedirectToAction("ParsingRulesForEmailProfile",new { id=parsingRule.EmailProfileId });
            }

            ViewBag.EmailProfileId = new SelectList(GetEmailProfilesForOrganizations(), "Id", "Name", parsingRule.EmailProfileId);
            return View(parsingRule);
        }

        // GET: ParsingRule/Edit/5
        public ActionResult Edit(int? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var parsingRule = db.ParsingRules.Find(id);
            if (parsingRule == null)
            {
                return HttpNotFound();
            }
            var list = new List<string>()
            {
                "Email",
                "FullName",
                "JobApplied"
            };
            

            ViewBag.CandidateProperties = new SelectList(list);
            ViewBag.EmailProfileId = new SelectList(GetEmailProfilesForOrganizations(), "Id", "Name", parsingRule.EmailProfileId);


            return View(parsingRule);
        }

        // POST: ParsingRule/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EmailField,Regex,IsQualificationRule,CandidateProperty,EmailProfileId")] ParsingRule parsingRule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(parsingRule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ParsingRulesForEmailProfile", new { id = parsingRule.EmailProfileId });
            }
            ViewBag.EmailProfileId = new SelectList(GetEmailProfilesForOrganizations(), "Id", "Name", parsingRule.EmailProfileId);
            return View(parsingRule);
        }

        // GET: ParsingRule/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var parsingRule = db.ParsingRules.Find(id);
            if (parsingRule == null)
            {
                return HttpNotFound();
            }
            return View(parsingRule);
        }

        // POST: ParsingRule/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var parsingRule = db.ParsingRules.Find(id);
            db.ParsingRules.Remove(parsingRule);
            db.SaveChanges();
            return RedirectToAction("ParsingRulesForEmailProfile", new { id = parsingRule.EmailProfileId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ParsingRulesForEmailProfile(int id)
        {

            var viewModel = new ShowParsingRulesViewModel();
            viewModel.ParsingRules = db.ParsingRules.Include(ep=>ep.EmailProfile).Where(p => p.EmailProfileId == id).ToList();
            if (User.IsInRole("OrgUser"))
            {
                viewModel.UserCanEdit = false;
            }
            else
            {
                viewModel.UserCanEdit = true;
            }
            return View("Index", viewModel);
        }



        private List<Organization> GetOrganizations()
        {
            if (User.IsInRole("SysAdmin"))
            {
                return db.Organizations.ToList();
            }
            var list = new List<Organization>();
            var userId = User.Identity.GetUserId();
            return db.Users.Include(o => o.Organizations).Single(u => u.Id == userId).Organizations.ToList();
        }

        private List<EmailProfile> GetEmailProfilesForOrganizations()
        {
            if (User.IsInRole("SysAdmin"))
            {
                return db.EmailProfiles.ToList();
            }
            var list = new List<EmailProfile>();
            var userId = User.Identity.GetUserId();

            foreach (var org in GetOrganizations())
            {

                list.AddRange(db.Organizations.Include(x => x.EmailProfiles).Single(ep => ep.Id == org.Id).EmailProfiles);
            }
            return list;
        }

        private List<ParsingRule> GetParsingRulesForEmailProfile()
        {
            var emailProfiles = GetEmailProfilesForOrganizations();

            var list = new List<ParsingRule>();

            foreach (var profile in emailProfiles)
            {
                list.AddRange(db.EmailProfiles.Include(x => x.ParsingRules).Single(pr => pr.Id == profile.Id).ParsingRules);
            }
            return list;
        }
    }
}
