﻿using System.Web.Mvc;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class TalentResourceManagerController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
