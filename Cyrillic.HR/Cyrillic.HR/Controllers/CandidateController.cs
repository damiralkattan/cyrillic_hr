﻿using CandidateEmailHandler;
using Cyrillic.HR.Models;
using Cyrillic.HR.Models.TalentResourceManager;
using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using Cyrillic.HR.Models.TalentResourceManager.Skills;
using Cyrillic.HR.ViewModels.TalentResourceManager;
using Microsoft.AspNet.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Cyrillic.HR.Controllers
{
    [Authorize]
    public class CandidateController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var candidates = new List<Candidate>();
            var orgs = GetOrganizations();

            foreach (var org in orgs)
            {
                var orgCandidates = GetCandidatesForOrganization(org.Id);

                //check if we are not showing same candidate already for a different org
                foreach (var candidate in orgCandidates)
                {
                    if (!candidates.Contains(candidate))
                    {
                        candidates.Add(candidate);
                    }
                }
            }

            var showCandidatesViewModel = new ShowCandidatesViewModel
            {
                Imported = false,
                Candidates = candidates
            };

            return View(showCandidatesViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var candidate = db.Candidates.Single(c => c.Id == id);

            db.Candidates.Remove(candidate);

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var candidate = db.Candidates.Include(o => o.Organizations).Include(s=>s.Skills).SingleOrDefault(c => c.Id == id);
            var removeOrganizations = GetOrganizations();
            var assignToOrganizations = GetOrganizations();
            var showCandidateOrganizations = candidate.Organizations.Intersect(GetOrganizations());

            //organizations that user can access to, that don`t have candidate already
            foreach (var org in candidate.Organizations)
            {

                assignToOrganizations.Remove(org);
            }

            if (!User.IsInRole("SysAdmin"))
            {
                removeOrganizations.Remove(db.Organizations.SingleOrDefault(x => x.Id == 1));
            }

            var candidateViewModel = new CandidateViewModel
            {
                Candidate = candidate,
                RemoveFromOrganizations = removeOrganizations,
                AssignToOrganizations = assignToOrganizations.ToList(),
                AccessableOrganizations = showCandidateOrganizations.ToList()

            };


            if (User.IsInRole("OrgUser"))
            {
                if (candidate.ApplicationUserId == User.Identity.GetUserId())
                {
                    candidateViewModel.EditableByUser = true;
                }
                else
                {
                    candidateViewModel.EditableByUser = false;
                }
            }
            else
            {
                candidateViewModel.EditableByUser = true;
            }


            var users = new List<ApplicationUser>();
            if (User.IsInRole("OrgAdmin") || User.IsInRole("SysAdmin"))
            {
                foreach (var org in GetOrganizations())
                {
                    users.AddRange(GetOrganizationUsers(org.Id));
                }
            }
            else
            {
                var userId = User.Identity.GetUserId();
                users.Add(db.Users.FirstOrDefault(x => x.Id == userId));
            }

            candidateViewModel.OrgUsers = users;

            return View(candidateViewModel);
        }

        [HttpPost]
        public ActionResult Edit(CandidateViewModel viewModel)
        {
            var addedOrg = db.Organizations.FirstOrDefault(x => x.Id == viewModel.SelectedOrganizationId);
            var removedOrg = db.Organizations.FirstOrDefault(x => x.Id == viewModel.RemovingOrganizationId);

            using (db)
            {
                var candidateToEdit = db.Candidates.Include(o => o.Organizations).SingleOrDefault(c => c.Id == viewModel.Candidate.Id);
                candidateToEdit.FirstName = viewModel.Candidate.FirstName;
                candidateToEdit.LastName = viewModel.Candidate.LastName;
                candidateToEdit.Email = viewModel.Candidate.Email;
                candidateToEdit.JobAppliedTo = candidateToEdit.JobAppliedTo != null ? candidateToEdit.JobAppliedTo : string.Empty;
                candidateToEdit.KeyWords = viewModel.Candidate.KeyWords;

                if (addedOrg != null && !candidateToEdit.Organizations.Contains(addedOrg))
                {
                    candidateToEdit.Organizations.Add(addedOrg);
                }

                if (removedOrg != null && candidateToEdit.Organizations.Contains(removedOrg))
                {
                    if (removedOrg.Id != 1)
                    {
                        candidateToEdit.Organizations.Remove(removedOrg);
                    }

                }
                candidateToEdit.ApplicationUserId = viewModel.AssignedOrgUserId;
                db.SaveChanges();
                db.Dispose();
            }



            return RedirectToAction("Edit", new { id = viewModel.Candidate.Id });
        }


        [HttpGet]
        public ActionResult Create()
        {
            var candidateVievModel = new CandidateViewModel
            {
                AccessableOrganizations = GetOrganizations()
            };

            return View(candidateVievModel);
        }

        [HttpPost]
        public ActionResult Create(CandidateViewModel viewModel)
        {

            var org = new List<Organization>()
            {
                db.Organizations.FirstOrDefault(x=>x.Name =="Global"),
                db.Organizations.FirstOrDefault(x=>x.Id==viewModel.SelectedOrganizationId)
            };
            

            var candidate = new Candidate
            {
                FirstName = viewModel.Candidate.FirstName,
                LastName = viewModel.Candidate.LastName,
                Email = viewModel.Candidate.Email,
                JobAppliedTo = viewModel.Candidate.JobAppliedTo != null ? viewModel.Candidate.JobAppliedTo : string.Empty,
                KeyWords = viewModel.Candidate.KeyWords,
                ImagePath = "DefaultUser.png",
                Organizations = org
            };

            db.Candidates.Add(candidate);

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ImportFromEmail()
        {
            var viewModel = new ImportFromEmailViewModel
            {

                EmailProfiles = GetEmailProfilesForOrganizations(),
                Organizations = GetOrganizations()
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult GetCandidates(ImportFromEmailViewModel viewModel)
        {
            var client = new EmailClient();
            var profile = db.EmailProfiles.First(p => p.Id == viewModel.SelectedEmailProfileId);
            foreach (var parsingRule in db.ParsingRules.Where(pr => pr.EmailProfileId == profile.Id))
            {
                profile.ParsingRules.Add(parsingRule);
            }

            var absoluteFolderPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, ConfigurationManager.AppSettings["local.resources.trm.candidates.attachments"]);
            client.Connect(profile.Username,
                           profile.Password,
                           ConfigurationManager.AppSettings["mail.client.hostname"],
                           profile.Port,
                          absoluteFolderPath);
            var parser = new EmailParser(client, profile.InboxName);
            var rules = profile.ParsingRules.Select(rule => new ParsingRuleDTO
            {
                EmailField = rule.EmailField,
                IsQualificationRule = rule.IsQualificationRule,
                Regex = rule.Regex,
                CandidateProperty = rule.CandidateProperty
            }).ToList();

            var persons = parser.GetCandidates(rules);

            var showCandidates = new ShowCandidatesViewModel();
            var candidates = new List<Candidate>();
            var orgs = new List<Organization>()
            {
                db.Organizations.FirstOrDefault(o=>o.Id==viewModel.SelectedOrganizationId)
            };
            foreach (var person in persons)
            {
                var candidate = new Candidate();
                var names = GetNames(person.FullName);
                candidate.Organizations = orgs;
                candidate.FirstName = ValidateName(names["FirstName"]);
                candidate.LastName = ValidateName(names["LastName"]);
                candidate.Email = person.Email;
                candidate.JobAppliedTo = person.JobApplied;

                candidates.Add(candidate);
            }
            showCandidates.ImportingOrganizationId = viewModel.SelectedOrganizationId;
            showCandidates.Candidates = candidates;
            showCandidates.Imported = true;

            return View("Index", showCandidates);
        }

        [HttpPost]
        public ActionResult SaveCandidates(ShowCandidatesViewModel viewModel)
        {

            var org = db.Organizations.SingleOrDefault(x => x.Id == viewModel.ImportingOrganizationId);
            var list = new List<Organization>() { org };
            foreach (var candidate in viewModel.Candidates)
            {
                db.Candidates.Add(new Candidate
                {
                    FirstName = candidate.FirstName ?? string.Empty,
                    LastName = candidate.LastName ?? string.Empty,
                    Email = candidate.Email ?? string.Empty,
                    JobAppliedTo = candidate.JobAppliedTo ?? string.Empty,
                    KeyWords = candidate.KeyWords,
                    ImagePath = "DefaultUser.png",
                    Organizations = list
                });
            }

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UploadSkills(int id)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/App_Data/"), fileName);
                    file.SaveAs(path);
                    SkillsParser parser = new SkillsParser();
                    var skills = parser.ReadAllSkills(path,"Skills Matrix");
                    var candidate = db.Candidates.Include(s => s.Skills).Single(x => x.Id == id);

                    foreach(var skill in skills)
                    {
                        candidate.Skills.Add(
                       new Skill()
                       {
                           Name = skill.Name,
                           Rating = skill.Rating,
                           YearsExperience = skill.YearsExperience,
                           SkillLevel = skill.SkillLevel
                       });
                    }
                   
                    db.SaveChanges();
                    db.Dispose();


                }
             
            }

           

            return RedirectToAction("Edit", new { id= id});

        }


        private List<Organization> GetOrganizations()
        {
            if (User.IsInRole("SysAdmin"))
            {
                return db.Organizations.ToList();
            }
            var list = new List<Organization>();
            var userId = User.Identity.GetUserId();
            return db.Users.Include(o => o.Organizations).Single(u => u.Id == userId).Organizations.ToList();
        }

        private List<EmailProfile> GetEmailProfilesForOrganizations()
        {
            if (User.IsInRole("SysAdmin"))
            {
                return db.EmailProfiles.ToList();
            }
            var list = new List<EmailProfile>();
            var userId = User.Identity.GetUserId();

            foreach (var org in GetOrganizations())
            {

                list.AddRange(db.Organizations.Include(x => x.EmailProfiles).Single(ep => ep.Id == org.Id).EmailProfiles);
            }
            return list;
        }

        private List<Candidate> GetCandidatesForOrganization(int id)
        {
            return db.Organizations.Include(x => x.Candidates).Include(us => us.ApplicationUsers).Single(x => x.Id == id).Candidates.ToList();
        }

        private List<ApplicationUser> GetOrganizationUsers(int id)
        {
            return db.Organizations.Include(x => x.ApplicationUsers).Single(au => au.Id == id).ApplicationUsers.ToList();
        }

        //handling first/last name from person full name property
        private static Dictionary<string, string> GetNames(string fullName)
        {
            var returnName = new Dictionary<string, string>();
            try
            {
                var names = fullName.Split((new[] { ' ' }), 2);
                returnName.Add(CandidateEmailHandler.Constants.FirstName, names[0]);
                returnName.Add(CandidateEmailHandler.Constants.LastName, names[1]);
                return returnName;
            }
            catch (IndexOutOfRangeException)
            {
                throw new IndexOutOfRangeException($"Tried to parse name: {fullName}, but it was not valid.");
            }
        }

        private static string ValidateName(string name)
        {
            var returnName = name.Trim('"', '*', '<', '>', '\'');
            return returnName.Trim();
        }
    }
}
