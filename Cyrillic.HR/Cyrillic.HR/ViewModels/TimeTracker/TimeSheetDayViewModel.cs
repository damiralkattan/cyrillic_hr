﻿using System;
using System.Collections.Generic;

namespace Cyrillic.HR.ViewModels.TimeTracker
{
    public class TimeSheetDayViewModel
    {
        public DateTime Date { get; set; }
        public string Name { get; set; }

        public List<TimeSheetTimeEntryViewModel> TimeSheetTimeEntries { get; set; }
    }
}
