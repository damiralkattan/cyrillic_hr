﻿namespace Cyrillic.HR.ViewModels.TimeTracker
{
    public class ReportProjectViewModel
    {
        public string Name { get; set; }
        public float TotalHours { get; set; }
    }
}
