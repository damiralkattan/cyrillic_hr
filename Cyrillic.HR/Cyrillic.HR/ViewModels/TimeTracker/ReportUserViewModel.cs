﻿namespace Cyrillic.HR.ViewModels.TimeTracker
{
    public class ReportUserViewModel
    {
        public string UserName { get; set; }
        public float TotalHoursRegular { get; set; }
        public float TotalHoursOvertime { get; set; }
    }
}
