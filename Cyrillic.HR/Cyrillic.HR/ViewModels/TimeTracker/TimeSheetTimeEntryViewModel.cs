﻿using Cyrillic.HR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Cyrillic.HR.Models.TimeTracker;

namespace Cyrillic.HR.ViewModels.TimeTracker
{
    public class TimeSheetTimeEntryViewModel
    {
        public DateTime WeekStartDate { get; set; }
        public DateTime WeekEndDate { get; set; }

        public int Id { get; set; }
        public string ApplicationUserId { get; set; }
        public DateTime Date { get; set; }

        #region Client DropdownList

        public int ClientId { get; set; }
        private readonly IEnumerable<Client> _clients;
        public IEnumerable<SelectListItem> Clients
        {
            get
            {
                var allClients = _clients.Select(c => new SelectListItem
                {
                    Value = c.Id.ToString(),
                    Text = c.Name
                });

                return DefaultClientItem.Concat(allClients);
            }
        }
        public IEnumerable<SelectListItem> DefaultClientItem => Enumerable.Repeat(new SelectListItem
        {
            Value = "-1",
            Text = "Client"
        }, count: 1);

        #endregion

        #region Project DropdownList

        public int ProjectId { get; set; }
        private readonly IEnumerable<Project> _projects;
        public IEnumerable<SelectListItem> Projects
        {
            get
            {
                var allProjects = _projects.Select(c => new SelectListItem
                {
                    Value = c.Id.ToString(),
                    Text = c.Name
                });

                return DefaultProjectItem.Concat(allProjects);
            }
        }
        public IEnumerable<SelectListItem> DefaultProjectItem => Enumerable.Repeat(new SelectListItem
        {
            Value = "-1",
            Text = "Project"
        }, count: 1);

        #endregion

        #region Task DropdownList

        public int ProjectTaskId { get; set; }
        private readonly IEnumerable<ProjectTask> _projectTasks;
        public IEnumerable<SelectListItem> ProjectTasks
        {
            get
            {
                var allProjectTasks = _projectTasks.Select(c => new SelectListItem
                {
                    Value = c.Id.ToString(),
                    Text = c.Name
                });

                return DefaultProjectTaskItem.Concat(allProjectTasks);
            }
        }
        public IEnumerable<SelectListItem> DefaultProjectTaskItem => Enumerable.Repeat(new SelectListItem
        {
            Value = "-1",
            Text = "Task"
        }, count: 1);

        #endregion

        public string Comment { get; set; }
        public float Hours { get; set; }
        public bool Overtime { get; set; }

        public TimeSheetTimeEntryViewModel()
        {
            _clients = new List<Client>();
            _projects = new List<Project>();
            _projectTasks = new List<ProjectTask>();
        }

        public TimeSheetTimeEntryViewModel(ApplicationDbContext db)
        {
            _clients = db.Clients.ToList();
            _projects = db.Projects.ToList();
            _projectTasks = db.ProjectTasks.ToList();
        }
    }
}
