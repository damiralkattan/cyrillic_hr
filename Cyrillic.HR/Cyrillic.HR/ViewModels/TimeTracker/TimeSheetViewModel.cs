﻿using System;
using System.Collections.Generic;
using Cyrillic.HR.Models;

namespace Cyrillic.HR.ViewModels.TimeTracker
{
    public class TimeSheetViewModel
    {
        public ApplicationDbContext Db { get; set; }

        public string ApplicationUserId { get; set; }
        public DateTime StartDay { get; set; }
        public DateTime EndDay { get; set; }
        public string StartDayFormatted { get; set; }
        public string EndDayFormatted { get; set; }

        public List<TimeSheetDayViewModel> TimeSheetDays { get; set; }
    }
}
