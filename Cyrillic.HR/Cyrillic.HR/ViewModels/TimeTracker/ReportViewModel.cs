﻿using System.Collections.Generic;

namespace Cyrillic.HR.ViewModels.TimeTracker
{
    public class ReportViewModel
    {
        public string MonthCustomChoice { get; set; }

        public Dictionary<string, string> Months { get; set; }
        public int SelectedMonthOrdinalNumber { get; set; }

        public string Start { get; set; }
        public string End { get; set; }

        public List<ReportProjectViewModel> Projects { get; set; }
        public List<ReportUserViewModel> Users { get; set; }
    }
}
