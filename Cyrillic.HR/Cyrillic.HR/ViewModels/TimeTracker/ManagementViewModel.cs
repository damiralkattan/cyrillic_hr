﻿namespace Cyrillic.HR.ViewModels.TimeTracker
{
    public class ManagementViewModel
    {
        public int NumberOfClients { get; set; }
        public int NumberOfProjects { get; set; }
        public int NumberOfProjectTasks { get; set; }
    }
}