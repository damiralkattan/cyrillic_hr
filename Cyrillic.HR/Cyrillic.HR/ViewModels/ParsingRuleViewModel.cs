﻿using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cyrillic.HR.ViewModels
{
    public class ParsingRuleViewModel
    {
        public ParsingRule ParsingRule { get; set; }

        public bool UserCanEdit { get; set; }
    }
}