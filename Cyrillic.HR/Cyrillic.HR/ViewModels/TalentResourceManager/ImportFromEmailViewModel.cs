﻿using Cyrillic.HR.Models.TalentResourceManager;
using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using System.Collections.Generic;

namespace Cyrillic.HR.ViewModels.TalentResourceManager
{
    public class ImportFromEmailViewModel
    {
        //public string Hostname { get; set; }
        //public string Username { get; set; }
        //public string Password { get; set; }
        //public List<string> Inboxes { get; set; }
        //public string Inbox { get; set; }
        public List<Organization> Organizations { get; set; }
        public int SelectedOrganizationId { get; set; }

        public List<EmailProfile> EmailProfiles { get; set; }
        public int SelectedEmailProfileId { get; set; }
    }
}
