﻿using Cyrillic.HR.Models;
using Cyrillic.HR.Models.TalentResourceManager;
using System.Collections.Generic;

namespace Cyrillic.HR.ViewModels.TalentResourceManager
{
    public class CandidateViewModel
    {
        public Candidate Candidate { get; set; }

        public int SelectedOrganizationId { get; set; }

        public int RemovingOrganizationId { get; set; }
        
        public List<Organization> RemoveFromOrganizations { get; set; }
        public List<Organization> AssignToOrganizations { get; set; }
        public List<Organization> AccessableOrganizations { get; set; }

        public ApplicationUser AssignedUser { get; set; }
        public bool EditableByUser { get; set; }
        public List<ApplicationUser> OrgUsers { get; set; }
        public string AssignedOrgUserId { get; set; }
    }
}
