﻿using Cyrillic.HR.Models.TalentResourceManager;
using System.Collections.Generic;

namespace Cyrillic.HR.ViewModels.TalentResourceManager
{
    public class ShowCandidatesViewModel
    {
        public IList<Candidate> Candidates { get; set; }
        public bool Imported { get; set; }
        public int ImportingOrganizationId {get;set;}
    }
}
