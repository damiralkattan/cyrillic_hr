﻿using System.Collections.Generic;
using Cyrillic.HR.Models.TalentResourceManager;

namespace Cyrillic.HR.ViewModels.TalentResourceManager
{
    public class OrganizationIndexViewModel
    {
        public bool CanManipulate { get; set; }
        public ICollection<Organization> Organizations { get; set; }
    }
}
