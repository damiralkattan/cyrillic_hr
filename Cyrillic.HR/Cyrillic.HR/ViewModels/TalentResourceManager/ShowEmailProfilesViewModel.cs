﻿using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cyrillic.HR.ViewModels.TalentResourceManager
{
    public class ShowEmailProfilesViewModel
    {
        public List<EmailProfile> EmailProfiles { get; set; }
        public bool UserCanEdit { get; set; }
    }
}