﻿using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cyrillic.HR.ViewModels
{
    public class ShowParsingRulesViewModel
    {

        public List<ParsingRule> ParsingRules { get; set; }
        public bool UserCanEdit { get; set; }
    }
}