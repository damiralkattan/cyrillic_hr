﻿using Cyrillic.HR.Models.TalentResourceManager;
using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using Cyrillic.HR.Models.TalentResourceManager.Skills;
using Cyrillic.HR.Models.TimeTracker;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Cyrillic.HR.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        // Custom Navigation Properties
        public ICollection<Organization> Organizations { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("ApplicationDbContext", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        #region Time Tracker Models

        public DbSet<Client> Clients { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> ProjectTasks { get; set; }
        public DbSet<TimeEntry> TimeEntries { get; set; }

        #endregion Time Tracker Models

        #region Talent Resource Manager Models

        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Skill> Skills { get; set; }

        #region Email Parsing Models

        public DbSet<EmailProfile> EmailProfiles { get; set; }
        public DbSet<ParsingRule> ParsingRules { get; set; }

        #endregion Email Parsing Models

        #endregion Talent Resource Manager Models

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region ApplicationUser Models Configuration

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(au => au.Organizations)
                .WithMany(o => o.ApplicationUsers)
                .Map(m =>
                {
                    m.ToTable("OrganizationUsers");
                    m.MapLeftKey("ApplicationUserId");
                    m.MapRightKey("OrganizationId");
                });

            #endregion

            #region Time Tracker Models Configuration

            #region Client Model Configuration

            modelBuilder.Entity<Client>().ToTable("Clients");
            modelBuilder.Entity<Client>().HasKey(c => c.Id);
            modelBuilder.Entity<Client>().Property(c => c.Name).IsRequired();
            modelBuilder.Entity<Client>()
                .HasMany(c => c.Projects)
                .WithRequired(p => p.Client)
                .HasForeignKey(p => p.ClientId);

            #endregion Client Model Configuration

            #region Project Model Configuration

            modelBuilder.Entity<Project>().ToTable("Projects");
            modelBuilder.Entity<Project>().HasKey(p => p.Id);
            modelBuilder.Entity<Project>().Property(p => p.Name).IsRequired();
            modelBuilder.Entity<Project>()
                .HasMany(p => p.ProjectTasks)
                .WithRequired(pt => pt.Project)
                .HasForeignKey(pt => pt.ProjectId);
            modelBuilder.Entity<Project>()
                .HasRequired(p => p.Client)
                .WithMany(c => c.Projects)
                .HasForeignKey(p => p.ClientId);

            #endregion Project Model Configuration

            #region ProjectTask Model Configuration

            modelBuilder.Entity<ProjectTask>().ToTable("ProjectTasks");
            modelBuilder.Entity<ProjectTask>().HasKey(pt => pt.Id);
            modelBuilder.Entity<ProjectTask>().Property(pt => pt.Name).IsRequired();
            modelBuilder.Entity<ProjectTask>()
                .HasRequired(pt => pt.Project)
                .WithMany(p => p.ProjectTasks)
                .HasForeignKey(pt => pt.ProjectId);

            #endregion ProjectTask Model Configuration

            #region TimeEntry Model Configuration

            modelBuilder.Entity<TimeEntry>().ToTable("TimeEntries");
            modelBuilder.Entity<TimeEntry>().HasKey(te => te.Id);
            modelBuilder.Entity<TimeEntry>().Property(te => te.Date).IsRequired();
            modelBuilder.Entity<TimeEntry>().Property(te => te.Comment).IsRequired();
            modelBuilder.Entity<TimeEntry>().Property(te => te.Hours).IsRequired();
            modelBuilder.Entity<TimeEntry>().Property(te => te.Overtime).IsRequired();
            modelBuilder.Entity<TimeEntry>()
                .HasRequired(te => te.ApplicationUser)
                .WithMany()
                .HasForeignKey(te => te.ApplicationUserId);
            modelBuilder.Entity<TimeEntry>()
                .HasRequired(te => te.Client)
                .WithMany()
                .HasForeignKey(te => te.ClientId);
            modelBuilder.Entity<TimeEntry>()
                .HasRequired(te => te.Project)
                .WithMany()
                .HasForeignKey(te => te.ProjectId)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<TimeEntry>()
                .HasRequired(te => te.ProjectTask)
                .WithMany()
                .HasForeignKey(te => te.ProjectTaskId)
                .WillCascadeOnDelete(false);

            #endregion TimeEntry Model Configuration

            #endregion Time Tracker Models Configuration

            #region Talent Resource Manager Models Configuration

            #region Candidate and Skills Models Configuration
            #region Candidate Model Configuration
            modelBuilder.Entity<Candidate>().ToTable("Candidates");
            modelBuilder.Entity<Candidate>().HasKey(c => c.Id);
            modelBuilder.Entity<Candidate>().Property(c => c.FirstName).IsRequired();
            modelBuilder.Entity<Candidate>().Property(c => c.LastName).IsRequired();
            modelBuilder.Entity<Candidate>().Property(c => c.Email).IsRequired();
            modelBuilder.Entity<Candidate>().Property(c => c.JobAppliedTo).IsRequired();
            modelBuilder.Entity<Candidate>()
                .HasMany(c => c.Organizations)
                .WithMany(o => o.Candidates)
                .Map(m =>
                {
                    m.ToTable("OrganizationCandidates");
                    m.MapLeftKey("CandidateId");
                    m.MapRightKey("OrganizationId");
                });
            modelBuilder.Entity<Candidate>()
                .HasOptional(c => c.ApplicationUser)
                .WithMany()
                .HasForeignKey(c => c.ApplicationUserId);
            modelBuilder.Entity<Candidate>()
              .HasMany(s => s.Skills)
              .WithRequired(c => c.Candidate)
              .HasForeignKey(s => s.CandidateId);
            #endregion Candidate Model Configuration

            #region Skills Model Configuration
            modelBuilder.Entity<Skill>().ToTable("Skills");
            modelBuilder.Entity<Skill>().HasKey(sk => sk.Id);
            modelBuilder.Entity<Skill>().Property(sk => sk.Name).IsRequired();
            modelBuilder.Entity<Skill>().Property(sk => sk.Rating);
            modelBuilder.Entity<Skill>().Property(sk => sk.SkillLevel);
            modelBuilder.Entity<Skill>().Property(sk => sk.YearsExperience);
            modelBuilder.Entity<Skill>()
                .HasRequired(sk => sk.Candidate)
                .WithMany(c => c.Skills)
                .HasForeignKey(sk => sk.CandidateId);
            #endregion SkillsModelConfiguration



            #endregion Candidate And Skills Models Configuration
            #region Organization Model Configuration

            modelBuilder.Entity<Organization>().ToTable("Organizations");
            modelBuilder.Entity<Organization>().HasKey(o => o.Id);
            modelBuilder.Entity<Organization>().Property(o => o.Name).IsRequired();
            modelBuilder.Entity<Organization>()
                .HasMany(o => o.Candidates)
                .WithMany(c => c.Organizations)
                .Map(m =>
                {
                    m.ToTable("OrganizationCandidates");
                    m.MapLeftKey("OrganizationId");
                    m.MapRightKey("CandidateId");
                });
            modelBuilder.Entity<Organization>().HasMany(o => o.EmailProfiles)
                .WithRequired(ep => ep.Organization)
                .HasForeignKey(ep => ep.OrganizationId);
            modelBuilder.Entity<Organization>()
                .HasMany(o => o.ApplicationUsers)
                .WithMany(au => au.Organizations)
                .Map(m =>
                {
                    m.ToTable("OrganizationUsers");
                    m.MapLeftKey("OrganizationId");
                    m.MapRightKey("ApplicationUserId");
                });

            #endregion Organization Model Configuration

            #region Email Parsing Rules Models Configuration

            #region Email Profile Model Configuration

            modelBuilder.Entity<EmailProfile>().ToTable("EmailProfiles");
            modelBuilder.Entity<EmailProfile>().HasKey(ep => ep.Id);
            modelBuilder.Entity<EmailProfile>().Property(ep => ep.Name).IsRequired();
            modelBuilder.Entity<EmailProfile>().Property(ep => ep.Hostname).IsRequired();
            modelBuilder.Entity<EmailProfile>().Property(ep => ep.Port).IsRequired();
            modelBuilder.Entity<EmailProfile>().Property(ep => ep.Username).IsRequired();
            modelBuilder.Entity<EmailProfile>().Property(ep => ep.Password).IsRequired();
            modelBuilder.Entity<EmailProfile>().Property(ep => ep.InboxName).IsRequired();
            modelBuilder.Entity<EmailProfile>()
                .HasRequired(ep => ep.Organization)
                .WithMany(o => o.EmailProfiles)
                .HasForeignKey(ep => ep.OrganizationId);
            modelBuilder.Entity<EmailProfile>()
                .HasMany(ep => ep.ParsingRules)
                .WithRequired(pr => pr.EmailProfile)
                .HasForeignKey(pr => pr.EmailProfileId);

            #endregion Email Profile Model Configuration

            #region Parsing Rule Model Configuration

            modelBuilder.Entity<ParsingRule>().ToTable("ParsingRules");
            modelBuilder.Entity<ParsingRule>().HasKey(pr => pr.Id);
            modelBuilder.Entity<ParsingRule>().Property(pr => pr.EmailField).IsRequired();
            modelBuilder.Entity<ParsingRule>().Property(pr => pr.Regex).IsRequired();
            modelBuilder.Entity<ParsingRule>().Property(pr => pr.IsQualificationRule).IsRequired();
            modelBuilder.Entity<ParsingRule>().Property(pr => pr.CandidateProperty);
            modelBuilder.Entity<ParsingRule>()
                .HasRequired(pr => pr.EmailProfile)
                .WithMany(ep => ep.ParsingRules)
                .HasForeignKey(pr => pr.EmailProfileId);

            #endregion Parsing Rule Model Configuration

            #endregion Email Parsing Rules Models Configuration

            #endregion Talent Resource Manager Models Configuration
        }
    }
}
