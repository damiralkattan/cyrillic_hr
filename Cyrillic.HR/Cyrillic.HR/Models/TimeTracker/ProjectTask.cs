﻿using System.ComponentModel;

namespace Cyrillic.HR.Models.TimeTracker
{
    public class ProjectTask
    {
        //Model Properties
        public int Id { get; set; }
        [DisplayName("Task Name")]
        public string Name { get; set; }

        //Navigation Properties
        [DisplayName("Project")]
        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
