﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Cyrillic.HR.Models.TimeTracker
{
    public class Project
    {
        //Model Properties
        public int Id { get; set; }
        [DisplayName("Project Name")]
        public string Name { get; set; }

        //Navigation Properties
        public ICollection<ProjectTask> ProjectTasks { get; set; }

        [DisplayName("Client")]
        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}
