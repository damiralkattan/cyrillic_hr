﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Cyrillic.HR.Models.TimeTracker
{
    public class Client
    {
        //Model Properties
        public int Id { get; set; }
        [DisplayName("Client Name")]
        public string Name { get; set; }

        //Navigation Properties
        public ICollection<Project> Projects { get; set; }
    }
}
