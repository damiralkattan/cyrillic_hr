﻿using System;
using System.ComponentModel;

namespace Cyrillic.HR.Models.TimeTracker
{
    public class TimeEntry
    {
        //Model Properties
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Comment { get; set; }
        public float Hours { get; set; }
        public bool Overtime { get; set; }

        //Navigation Properties
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        [DisplayName("Client")]
        public int ClientId { get; set; }
        public Client Client { get; set; }

        [DisplayName("Project")]
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        [DisplayName("Task")]
        public int ProjectTaskId { get; set; }
        public ProjectTask ProjectTask { get; set; }
    }
}
