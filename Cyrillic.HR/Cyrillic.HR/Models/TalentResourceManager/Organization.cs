﻿using System.Collections.Generic;
using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;

namespace Cyrillic.HR.Models.TalentResourceManager
{
    public class Organization
    {
        // Model Properties
        public int Id { get; set; }
        public string Name { get; set; }
        
        // Navigation Properties
        public ICollection<Candidate> Candidates { get; set; }

        public ICollection<EmailProfile> EmailProfiles { get; set; }

        public ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}
