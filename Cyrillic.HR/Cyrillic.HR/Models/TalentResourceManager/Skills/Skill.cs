﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cyrillic.HR.Models.TalentResourceManager.Skills
{
    public class Skill
    {
        // Model Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public float Rating { get; set; }
        public float YearsExperience { get; set; }
        public string SkillLevel { get; set; }

        // Navigation Properties
        public Candidate Candidate { get; set; }
        public int CandidateId { get; set; }
       
    }
}