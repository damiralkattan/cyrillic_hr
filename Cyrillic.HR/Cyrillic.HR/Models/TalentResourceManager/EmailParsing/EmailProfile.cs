﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Cyrillic.HR.Models.TalentResourceManager.EmailParsing
{
    public class EmailProfile
    {
        // Model Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string Hostname { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        [DisplayName("Inbox")]
        public string InboxName { get; set; }

        // Navigation Properties
        [DisplayName("Organization")]
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }

        public ICollection<ParsingRule> ParsingRules { get; set; }
    }
}
