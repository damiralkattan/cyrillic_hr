﻿using System.ComponentModel;

namespace Cyrillic.HR.Models.TalentResourceManager.EmailParsing
{
    public class ParsingRule
    {
        // Model Properties
        public int Id { get; set; }
        [DisplayName("Email Field")]
        public string EmailField { get; set; }
        public string Regex { get; set; }
        [DisplayName("Qualification Rule")]
        public bool IsQualificationRule { get; set; }
        [DisplayName("Candidate Property")]
        public string CandidateProperty { get; set; }

        // Navigation Properties
        [DisplayName("Email Profile")]
        public int EmailProfileId { get; set; }
        public EmailProfile EmailProfile { get; set; }
    }
}
