﻿using Cyrillic.HR.Models.TalentResourceManager.Skills;
using System.Collections;
using System.Collections.Generic;

namespace Cyrillic.HR.Models.TalentResourceManager
{
    public class Candidate
    {
        // Model Properties
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string KeyWords { get; set; }
        public string JobAppliedTo { get; set; }
        public string ImagePath { get; set; }

        // Navigation Properties


        public ICollection<Organization> Organizations { get; set; }
        public ICollection<Skill> Skills { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
