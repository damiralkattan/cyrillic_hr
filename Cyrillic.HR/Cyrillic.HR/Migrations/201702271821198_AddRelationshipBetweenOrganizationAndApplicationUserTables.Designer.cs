// <auto-generated />
namespace Cyrillic.HR.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddRelationshipBetweenOrganizationAndApplicationUserTables : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddRelationshipBetweenOrganizationAndApplicationUserTables));
        
        string IMigrationMetadata.Id
        {
            get { return "201702271821198_AddRelationshipBetweenOrganizationAndApplicationUserTables"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
