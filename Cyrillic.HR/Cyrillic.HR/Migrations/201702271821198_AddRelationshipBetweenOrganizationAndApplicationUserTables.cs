namespace Cyrillic.HR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationshipBetweenOrganizationAndApplicationUserTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrganizationUsers",
                c => new
                    {
                        OrganizationId = c.Int(nullable: false),
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.OrganizationId, t.ApplicationUserId })
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .Index(t => t.OrganizationId)
                .Index(t => t.ApplicationUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrganizationUsers", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrganizationUsers", "OrganizationId", "dbo.Organizations");
            DropIndex("dbo.OrganizationUsers", new[] { "ApplicationUserId" });
            DropIndex("dbo.OrganizationUsers", new[] { "OrganizationId" });
            DropTable("dbo.OrganizationUsers");
        }
    }
}
