namespace Cyrillic.HR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class RemoveEmailDetailsFromOrganization : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Organizations", "EmailHostname");
            DropColumn("dbo.Organizations", "EmailAddress");
            DropColumn("dbo.Organizations", "EmailPassword");
            DropColumn("dbo.Organizations", "EmailInboxes");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Organizations", "EmailInboxes", c => c.String(nullable: false));
            AddColumn("dbo.Organizations", "EmailPassword", c => c.String(nullable: false));
            AddColumn("dbo.Organizations", "EmailAddress", c => c.String(nullable: false));
            AddColumn("dbo.Organizations", "EmailHostname", c => c.String(nullable: false));
        }
    }
}
