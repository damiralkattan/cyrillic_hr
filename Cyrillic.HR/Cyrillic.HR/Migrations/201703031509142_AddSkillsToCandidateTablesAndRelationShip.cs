namespace Cyrillic.HR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSkillsToCandidateTablesAndRelationShip : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Skills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Rating = c.Single(nullable: false),
                        YearsExperience = c.Single(nullable: false),
                        SkillLevel = c.String(),
                        CandidateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidates", t => t.CandidateId, cascadeDelete: true)
                .Index(t => t.CandidateId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Skills", "CandidateId", "dbo.Candidates");
            DropIndex("dbo.Skills", new[] { "CandidateId" });
            DropTable("dbo.Skills");
        }
    }
}
