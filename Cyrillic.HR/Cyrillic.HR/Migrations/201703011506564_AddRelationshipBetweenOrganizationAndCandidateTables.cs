namespace Cyrillic.HR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationshipBetweenOrganizationAndCandidateTables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Candidates", "OrganizationId", "dbo.Organizations");
            DropIndex("dbo.Candidates", new[] { "OrganizationId" });
            CreateTable(
                "dbo.OrganizationCandidates",
                c => new
                    {
                        CandidateId = c.Int(nullable: false),
                        OrganizationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CandidateId, t.OrganizationId })
                .ForeignKey("dbo.Candidates", t => t.CandidateId, cascadeDelete: true)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .Index(t => t.CandidateId)
                .Index(t => t.OrganizationId);
            
            DropColumn("dbo.Candidates", "OrganizationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Candidates", "OrganizationId", c => c.Int(nullable: false));
            DropForeignKey("dbo.OrganizationCandidates", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationCandidates", "CandidateId", "dbo.Candidates");
            DropIndex("dbo.OrganizationCandidates", new[] { "OrganizationId" });
            DropIndex("dbo.OrganizationCandidates", new[] { "CandidateId" });
            DropTable("dbo.OrganizationCandidates");
            CreateIndex("dbo.Candidates", "OrganizationId");
            AddForeignKey("dbo.Candidates", "OrganizationId", "dbo.Organizations", "Id", cascadeDelete: true);
        }
    }
}
