namespace Cyrillic.HR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class CreateTalentResourceManagerTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        KeyWords = c.String(),
                        JobAppliedTo = c.String(nullable: false),
                        ImagePath = c.String(),
                        OrganizationId = c.Int(nullable: false),
                        ApplicationUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .Index(t => t.OrganizationId)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        EmailHostname = c.String(nullable: false),
                        EmailAddress = c.String(nullable: false),
                        EmailPassword = c.String(nullable: false),
                        EmailInboxes = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Candidates", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.Candidates", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Candidates", new[] { "ApplicationUserId" });
            DropIndex("dbo.Candidates", new[] { "OrganizationId" });
            DropTable("dbo.Organizations");
            DropTable("dbo.Candidates");
        }
    }
}
