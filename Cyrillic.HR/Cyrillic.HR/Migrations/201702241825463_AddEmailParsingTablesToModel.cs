namespace Cyrillic.HR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailParsingTablesToModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Hostname = c.String(nullable: false),
                        Port = c.Int(nullable: false),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        InboxName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ParsingRules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmailField = c.String(nullable: false),
                        Regex = c.String(nullable: false),
                        IsQualificationRule = c.Boolean(nullable: false),
                        CandidateProperty = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ParsingRules");
            DropTable("dbo.EmailProfiles");
        }
    }
}
