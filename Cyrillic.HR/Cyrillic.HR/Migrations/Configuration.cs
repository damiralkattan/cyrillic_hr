using System.Data.Entity;
using Cyrillic.HR.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using Cyrillic.HR.Models.TalentResourceManager;

namespace Cyrillic.HR.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            #region Role Creation

            #region Create Admin Role

            if (!context.Roles.Any(r => r.Name == "SysAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole
                {
                    Name = "SysAdmin"
                };

                manager.Create(role);
            }

            #endregion

            #region Create OrgAdmin Role

            if (!context.Roles.Any(r => r.Name == "OrgAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole
                {
                    Name = "OrgAdmin"
                };

                manager.Create(role);
            }
            #endregion

            #region Create OrgUser Role

            if (!context.Roles.Any(r => r.Name == "OrgUser"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole
                {
                    Name = "OrgUser"
                };

                manager.Create(role);
            }

            #endregion

            #endregion

            #region Organization Creation

            #region Create Global Organization

            if (!context.Organizations.Any(o => o.Name == "Global"))
            {
                var organization = new Organization
                {
                    Name = "Global"
                };

                context.Organizations.Add(organization);
                context.SaveChanges();
            }

            #endregion

            #endregion

            #region User Creation

            #region Create Admin User

            if (context.Users.Any(u => u.UserName == "Admin")) return;
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser
                {
                    UserName = "Admin",
                    Email = "admin@cyrillicsoftware.net",
                    EmailConfirmed = true
                };

                manager.Create(user, "Admin@1");
                manager.AddToRole(user.Id, "SysAdmin");

                var allOrganization = context.Organizations.Single(o => o.Name == "Global");
                var adminUser = context.Users.Include(u => u.Organizations).Single(u => u.UserName == "Admin");
                adminUser.Organizations.Add(allOrganization);
                context.SaveChanges();
            }

            #endregion

            #endregion
        }
    }
}
