namespace Cyrillic.HR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationshipBetweenEmailProfileAndParsingRuleTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ParsingRules", "EmailProfileId", c => c.Int(nullable: false));
            CreateIndex("dbo.ParsingRules", "EmailProfileId");
            AddForeignKey("dbo.ParsingRules", "EmailProfileId", "dbo.EmailProfiles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParsingRules", "EmailProfileId", "dbo.EmailProfiles");
            DropIndex("dbo.ParsingRules", new[] { "EmailProfileId" });
            DropColumn("dbo.ParsingRules", "EmailProfileId");
        }
    }
}
