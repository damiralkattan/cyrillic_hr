namespace Cyrillic.HR.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationshipBetweenEmailProfileAndOrganizationTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailProfiles", "OrganizationId", c => c.Int(nullable: false));
            CreateIndex("dbo.EmailProfiles", "OrganizationId");
            AddForeignKey("dbo.EmailProfiles", "OrganizationId", "dbo.Organizations", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmailProfiles", "OrganizationId", "dbo.Organizations");
            DropIndex("dbo.EmailProfiles", new[] { "OrganizationId" });
            DropColumn("dbo.EmailProfiles", "OrganizationId");
        }
    }
}
