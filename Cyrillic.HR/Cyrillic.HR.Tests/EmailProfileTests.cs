﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cyrillic.HR.Controllers;
using System.Web.Mvc;
using Cyrillic.HR.Models;
using System.Linq;
using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;

namespace Cyrillic.HR.Tests
{
    /// <summary>
    /// Summary description for EmailProfileTests
    /// </summary>
    [TestClass]
    public class EmailProfileTests
    {
      
        [TestMethod]
        public void TestEmailProfileIndex()
        {
            var controller = new EmailProfileController();
            var result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestEmailProfileDetails()
        {
            var controller = new EmailProfileController();
            ApplicationDbContext db = new ApplicationDbContext();
            var emailProfile = db.EmailProfiles.FirstOrDefault();

            var result = controller.Details(emailProfile.Id) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestEmailProfileCreate()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var controller = new EmailProfileController();
            var emailProfile = new EmailProfile()
            {
                Name = "UnitTestEP",
                Id = 0,
                Hostname = "",
                Port = 123,
                Username = "",
                Password = "",
                InboxName = "",
                OrganizationId = 1,
            };
            var result = controller.Create(emailProfile) as RedirectToRouteResult;
            var check = db.EmailProfiles.Any(ep => ep.Name == "UnitTestEP");

            Assert.IsTrue(result.RouteValues["action"].ToString().Equals("Index") && check);
        }

        [TestMethod]
        public void TestEmailProfileDelete()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var controller = new EmailProfileController();

            var emailProfileId = db.EmailProfiles.FirstOrDefault(x => x.Name == "UnitTestEP").Id;

            var result = controller.Delete(emailProfileId) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestEmailProfileDeleteConfirmed()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var controller = new EmailProfileController();

            var emailProfileId = db.EmailProfiles.FirstOrDefault(x => x.Name == "UnitTestEP").Id;

            var result = controller.DeleteConfirmed(emailProfileId) as RedirectToRouteResult;
            Assert.IsTrue(result.RouteValues["action"].ToString().Equals("Index"));

        }

        [TestMethod]
        public void TestEmailProfileEdit()
        {
            var controller = new EmailProfileController();
            ApplicationDbContext db = new ApplicationDbContext();
            var emailProfileId = db.EmailProfiles.FirstOrDefault().Id;

            var result = controller.Edit(emailProfileId) as ViewResult;

            Assert.IsNotNull(result);
        }
    }
}
