﻿using Cyrillic.HR.Controllers;
using Cyrillic.HR.Models;
using Cyrillic.HR.Models.TalentResourceManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cyrillic.HR.Tests
{
    [TestClass]
    public class OrganizationControllerTests
    {

        [TestMethod]
        public void TestOrganizationControllerDetails()
        {
            var controller = new OrganizationController();
            ApplicationDbContext db = new ApplicationDbContext();
            var org = db.Organizations.FirstOrDefault();

            var result = controller.Details(org.Id) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestOrganizationControllerCreate()
        {
            var controller = new OrganizationController();
            var org = new Organization()
            {
                Id = 100,
                Name = "UnitTestOrg"
            };
            var result = controller.Create(org) as RedirectToRouteResult;

            Assert.AreEqual("Index", result.RouteValues["action"].ToString());
        }

        [TestMethod]
        public void TestOrganizationControllerDelete()
        {
            var controller = new OrganizationController();
            ApplicationDbContext db = new ApplicationDbContext();
            var org = db.Organizations.FirstOrDefault(x => x.Name == "UnitTestOrg");

            var result = controller.Delete(org.Id) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestOrganizationControllerDeleteConfirmed()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var controller = new OrganizationController();

            var orgId = db.Organizations.FirstOrDefault(x => x.Name == "UnitTestOrg").Id;

            var result = controller.DeleteConfirmed(orgId) as RedirectToRouteResult;
            Assert.IsTrue(result.RouteValues["action"].ToString().Equals("Index"));
        }
    }
}
