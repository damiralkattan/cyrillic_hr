﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cyrillic.HR.Controllers;
using System.Web.Mvc;
using Cyrillic.HR.Models.TalentResourceManager;
using Cyrillic.HR.ViewModels.TalentResourceManager;
using System.Data.SqlClient;
using Cyrillic.HR.Models;
using System.Linq;
using System.Collections.Generic;

namespace Cyrillic.HR.Tests
{
    [TestClass]
    public class CandidateControllerTests
    {
        [TestMethod]
        public void TestCandidateControllerIndex()
        {
            var controller = new CandidateController();
            var result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestCandidateControllerCreate()
        {
            var controller = new CandidateController();
            var candidate = new Candidate()
            {
                Id = 0,
                FirstName = "UnitTestCandidateFirstName",
                LastName = "UnitTestCandidateLastName",
                Email = "Unit@Test.email",
                JobAppliedTo = "Unit Tester",
                Organizations = new List<Organization> { ApplicationDbContext.Create().Organizations.FirstOrDefault(o => o.Id == 1) }
            };
            var candidateViewModel = new CandidateViewModel();
            candidateViewModel.Candidate = candidate;
            candidateViewModel.SelectedOrganizationId = 1;

            var result = controller.Create(candidateViewModel) as RedirectToRouteResult;

            Assert.AreEqual("Index", result.RouteValues["action"].ToString());
        }

        [TestMethod]
        public void TestCandidateControllerDelete()
        {
            var controller = new CandidateController();
            ApplicationDbContext db = new ApplicationDbContext();
            var candidateId = db.Candidates.FirstOrDefault(c => c.FirstName == "UnitTestCandidateFirstName" ).Id;

            var result = controller.Delete(candidateId) as RedirectToRouteResult;
            var check = db.Candidates.Any(c => c.Id == candidateId);

            Assert.IsTrue(result.RouteValues["action"].ToString().Equals("Index") && !check);
        }

        [TestMethod]
        public void TestCandidateControllerSaveCandidates()
        {
            var model = new ShowCandidatesViewModel();
            var controller = new CandidateController();
            ApplicationDbContext db = new ApplicationDbContext();
            model.Candidates = new List<Candidate>
            {
                new Candidate()
                {
                    Id = 0,
                    FirstName = "UnitTestCandidateFirstName",
                    LastName = "UnitTestCandidateLastName",
                    Email = "Unit@Test.email",
                    JobAppliedTo = "Unit Tester",
                    Organizations = new List<Organization> { ApplicationDbContext.Create().Organizations.FirstOrDefault(o => o.Id == 1) }
                },

                new Candidate()
                {
                    Id = 0,
                    FirstName = "UnitTestCandidateFirstNameSaveCandidates",
                    LastName = "UnitTestCandidateLastName",
                    Email = "Unit@Test.email",
                    JobAppliedTo = "Unit Tester",
                    Organizations = new List<Organization> { ApplicationDbContext.Create().Organizations.FirstOrDefault(o => o.Id == 1) }
                }
            };
           
            var result = controller.SaveCandidates(model) as RedirectToRouteResult;
            var check = db.Candidates.Any(c => c.FirstName == "UnitTestCandidateFirstNameSaveCandidates");
            Assert.IsTrue(result.RouteValues["action"].ToString().Equals("Index") && check);
        }
    }
}
