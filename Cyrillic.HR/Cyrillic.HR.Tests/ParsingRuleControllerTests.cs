﻿using Cyrillic.HR.Controllers;
using Cyrillic.HR.Models;
using Cyrillic.HR.Models.TalentResourceManager.EmailParsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cyrillic.HR.Tests
{
    [TestClass]
    public class ParsingRuleControllerTests
    {

        [TestMethod]
        public void TestParsingRuleControllerIndex()
        {
            var controller = new ParsingRuleController();
            var result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestParsingRuleCreate()
        {
            var controller = new ParsingRuleController();
            var rule = new ParsingRule()
            {
                Id=0,
                IsQualificationRule=false,
                EmailField="",
                EmailProfileId=1,
                CandidateProperty="UnitTest",
                Regex="SomeRegex"
            };
            var result = controller.Create(rule) as RedirectToRouteResult;

            Assert.AreEqual("Index", result.RouteValues["action"].ToString());
        }

        [TestMethod]
        public void TestParsingRuleControllerDeleteConfirmed()
        {
            var controller = new ParsingRuleController();
            ApplicationDbContext db = new ApplicationDbContext();
            var ruleId = db.ParsingRules.FirstOrDefault(c => c.CandidateProperty == "UnitTest").Id;

            var result = controller.DeleteConfirmed(ruleId) as RedirectToRouteResult;
            var check = db.ParsingRules.Any(pr => pr.CandidateProperty == "UnitTest");
            Assert.IsTrue(result.RouteValues["action"].ToString().Equals("Index") && !check);
        }


    }
}
