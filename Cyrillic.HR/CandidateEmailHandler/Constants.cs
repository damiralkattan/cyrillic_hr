﻿namespace CandidateEmailHandler
{
    public static class Constants
    {
        //public static string XmlProvidersConfig = @"C:\Users\Damir\Documents\Visual Studio 2015\Projects\EmailHandler\EmailHandler\Template.xml";
        //public static string XmlAttachmentConfig = @"C:\Users\Damir\Documents\Visual Studio 2015\Projects\EmailHandler\EmailHandler\AttachmentTemplate.xml";
        public static string XmlProvidersConfig = @"D:\Projects\Cyrillic_Software\cyrillic_hr\Cyrillic.HR\CandidateEmailHandler\Template.xml";
        public static string XmlAttachmentConfig = @"D:\Projects\Cyrillic_Software\cyrillic_hr\Cyrillic.HR\CandidateEmailHandler\AttachmentTemplate.xml";
        public static string EmailAdress = "recruiting@cyrillicsoftware.net";
        public static string EmailPassword = "J90wIej209";
        public static string EmailHost = "mail.cyrillicsoftware.net";
        public static string FromConditionKey = "From";
        public static string RegexKey = "Regex";
        public static string XPathEmailFrom = "{0}/Rules/CandidateEmail/From";
        public static string XPathEmailRegex = "{0}/Rules/CandidateEmail/Regex";
        public static string XpathNameFrom = "{0}/Rules/CandidateName/From";
        public static string XpathNameRegex = "{0}/Rules/CandidateName/Regex";
        public static string XpathProviderRuleFrom = "//{0}/QualificationRules/From";
        public static string XpathProviderRuleRegex = "//{0}/QualificationRules/Regex";
        public static string XpathAttachmentEmailFrom = "//Attachment/CandidateEmail/From";
        public static string XpathAttachmentEmailRegex = "//Attachment/CandidateEmail/Regex";

        public static string FirstName = "FirstName";
        public static string LastName = "LastName";
    }
}
