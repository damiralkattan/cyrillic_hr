﻿using S22.Imap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace CandidateEmailHandler
{
    /// <summary>
    /// Handle download of Email Messages and Attachments 
    /// </summary>
    public class EmailClient
    {
        private ImapClient _client;
        private string _hostname = string.Empty;
        public string _attachmentFolder;

        /// <summary>
        /// Connect to the email server using username and password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="hostname"></param>
        /// <returns></returns>
        public Boolean Connect(string username, string password, string hostname, int port,string attachmentFolder)
        {
            _hostname = hostname;
            _attachmentFolder = attachmentFolder;
            try
            {
                _client = new ImapClient(hostname, port,
                 username, password, AuthMethod.Login, true);
                return _client.Authed;
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException("Email credentials cannot be null!");
            }

        }

        /// <summary>
        /// Get all the attachments in the selected inbox to the destination folder
        /// </summary>
        /// <param name="inbox"></param>
        /// <param name="destinationFolder"></param>
        /// <returns></returns>
        public List<string> GetAttachments(MailMessage msg)
        {
            var attachments = new List<string>();
            using (_client)
            {
                try
                {
                    foreach (var att in msg.Attachments)
                    {
                        try
                        {
                            DownloadAttachment(att,_attachmentFolder);
                            attachments.Add(Path.Combine(_attachmentFolder, att.ContentDisposition.FileName));
                        }
                        catch (DirectoryNotFoundException)
                        {
                            throw new DirectoryNotFoundException(string.Format("Destination folder {0} not found"));
                        }
                    }

                }
                catch (BadServerResponseException)
                {
                    throw new BadServerResponseException("Message or Inbox not found!");
                }
            }
            return attachments;
        }


        /// <summary>
        /// Get all Email Messages in the selected Inbox
        /// </summary>
        /// <param name="inbox"></param>
        /// <returns>IEnumerable<MailMessage></returns>
        public IEnumerable<MailMessage> GetMessages(string inbox)
        {
            IEnumerable<MailMessage> messages;
            using (_client)
            {
                try
                {
                    var uids = _client.Search(SearchCondition.All(), inbox);
                    messages = _client.GetMessages(uids, true, inbox);
                }
                catch (BadServerResponseException)
                {
                    throw new BadServerResponseException(string.Format("Inbox {0} not found!", inbox));
                }
            }
            return messages;
        }


        /// <summary>
        /// Private method for downloading attachments into destination folder
        /// </summary>
        /// <param name="attachment"></param>
        /// <param name="desinationFolder"></param>
        /// <returns></returns>
        private string DownloadAttachment(Attachment attachment, string desinationFolder)
        {
            var allBytes = new byte[attachment.ContentStream.Length];
            var bytesRead = attachment.ContentStream.Read(allBytes, 0, (int)attachment.ContentStream.Length);

            //save files in attchments folder
            var destinationFile = Path.Combine(desinationFolder, attachment.Name);
            var writer = new BinaryWriter(new FileStream(destinationFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None));
            writer.Write(allBytes);
            writer.Close();
            return destinationFile;
        }

    }
}
