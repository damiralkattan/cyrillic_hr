﻿
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;


namespace CandidateEmailHandler
{
    public class SkillsParser
    {
        public string Name { get; set; }
        public float Rating { get; set; }
        public float YearsExperience { get; set; }
        public string SkillLevel { get; set; }


        //read Excel file into string
        public List<SkillDto> ReadSkills(string fileName, string sheetName, List<string> skills)
        {
            if (!File.Exists(fileName))
            {
                return null;
            }

            var returnListDto = new List<SkillDto>();

            var connectionString = string.Empty;

            if (Path.GetExtension(fileName) == ".xls")
            {
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            }
            else if (Path.GetExtension(fileName) == ".xlsx")
            {
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }

            string query = string.Format("SELECT * FROM [{0}$]", sheetName);

            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                OleDbCommand command = new OleDbCommand(query, connection);
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var value = reader[0].ToString();
                    if (skills.Any(s => s.Equals(value, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        var skill = new SkillDto();
                        skill.Name = value;
                        skill.Rating = GetFloatFromString(reader[1].ToString());
                        skill.YearsExperience = GetFloatFromString(reader[2].ToString());
                        skill.SkillLevel = reader[3].ToString();
                        returnListDto.Add(skill);
                    }
                }
                reader.Close();

                return returnListDto;

            }
        }

        public List<SkillDto> ReadAllSkills(string fileName, string sheetName)
        {
            if (!File.Exists(fileName))
            {
                return null;
            }

            var returnListDto = new List<SkillDto>();

            var connectionString = string.Empty;

            if (Path.GetExtension(fileName) == ".xls")
            {
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            }
            else if (Path.GetExtension(fileName) == ".xlsx")
            {
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }

            string query = string.Format("SELECT * FROM [{0}$]", sheetName);

            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                OleDbCommand command = new OleDbCommand(query, connection);
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var value = reader[0].ToString();
                    if (CheckSkillCells(reader))
                    {
                        var skill = new SkillDto();
                        skill.Name = value;
                        skill.Rating = GetFloatFromString(reader[1].ToString());
                        skill.YearsExperience = GetFloatFromString(reader[2].ToString());
                        skill.SkillLevel = reader[3].ToString();
                        returnListDto.Add(skill);
                    }

                }


                reader.Close();

                return returnListDto;

            }
        }


        private float GetFloatFromString(string input)
        {
            if (input != string.Empty)
            {
                return float.Parse(input, CultureInfo.InvariantCulture.NumberFormat);
            }
            else
            {
                return 0f;
            }

        }

        private Boolean CheckSkillCells(OleDbDataReader reader)
        {
            if (reader.FieldCount == 4)
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    if (reader[i].ToString() == null || reader[i].ToString() == string.Empty)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
