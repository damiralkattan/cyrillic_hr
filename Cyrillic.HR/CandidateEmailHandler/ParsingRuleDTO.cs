﻿using System;

namespace CandidateEmailHandler
{
    public class ParsingRuleDTO
    {
        public string EmailField { get; set; }
        public string Regex { get; set; }
        public string CandidateProperty { get; set; }
        public bool IsQualificationRule { get; set;}
    }
}
