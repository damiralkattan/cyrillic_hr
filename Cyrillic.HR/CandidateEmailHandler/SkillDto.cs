﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandidateEmailHandler
{
    public class SkillDto
    {

        public string Name { get; set; }
        public float Rating { get; set; }
        public float YearsExperience { get; set; }
        public string SkillLevel { get; set; }
    }
}
