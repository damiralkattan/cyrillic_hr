﻿using IFilterTextReader;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.IO;
using System.Text;

namespace CandidateEmailHandler
{

    public class AttachmentHandler
    {

        private string _filePath;


        /// <summary>
        /// Public constructor for Attachment Handler
        /// </summary>
        /// <param name="filePath"></param>
        public AttachmentHandler(string filePath)
        {
            if (File.Exists(filePath))
            {
                _filePath = filePath;
            }
            else
            {
                throw new FileNotFoundException("File not found :" + filePath);
            }

        }

        /// <summary>
        /// Main method for reading the documents
        /// </summary>
        public string ReadFile()
        {

            var ext = System.IO.Path.GetExtension(_filePath);

            switch (ext)
            {
                case (".doc"):
                    return ReadDOC();

                case (".pdf"):
                    return ReadPDF();

                case (".docx"):
                    return ReadDOC();

                default:
                    throw new Exception("File Format not supported for the file" + _filePath);
            }
        }
        //reads PDF files into string
        private string ReadPDF()
        {
            using (var reader = new PdfReader(_filePath))
            {
                var text = new StringBuilder();
                for (var i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }
                return text.ToString();
            }
        }

        //reads DOC/DOCX files into string
        private string ReadDOC()
        {
            TextReader reader = new FilterReader(_filePath);
            using (reader)
            {
                return reader.ReadToEnd();
            };
        }




    }

}
