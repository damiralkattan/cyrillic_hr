﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CandidateEmailHandler
{
    /// <summary>
    /// Helper class for parsing the Body of  Email Messages into Person objects
    /// </summary>
    public class EmailParser
    {
        private IEnumerable<MailMessage> _msgs;
        private string _attachment = string.Empty;
        EmailClient _client;

        /// <summary>
        /// Constructor for EmailParser class accepting the connected EmailClient object and desired Inbox name
        /// </summary>
        /// <param name="client"></param>
        /// <param name="inbox"></param>
        public EmailParser(EmailClient client, string inbox)
        {
            _client = client;
            _msgs = client.GetMessages(inbox);
        }

        /// <summary>
        /// Main method for getting the List of Person objects found in Email Messages
        /// </summary>
        /// <returns>List<Person></returns>
        public List<Person> GetCandidates(List<ParsingRuleDTO> rules)
        {
            var ParseRules = rules.Where(rule => rule.IsQualificationRule == false);
            var QualifyingRules = rules.Where(rule => rule.IsQualificationRule == true);
            var list = new List<Person>();
            if (QualifyingRules.Any())
            {
                foreach (var msg in _msgs)
                {
                    if (CheckQualificationRules(QualifyingRules, msg))
                    {
                        var person = new Person();
                        foreach (var rule in ParseRules)
                        {
                            var value = ApplyCondition(rule, msg);
                            var type = person.GetType();
                            var prop = type.GetProperty(rule.CandidateProperty);
                            prop.SetValue(person, value, null);
                        }
                        list.Add(person);
                    }
                }
               
            }
            return list;
        }


        private bool CheckQualificationRules(IEnumerable<ParsingRuleDTO> rules, MailMessage msg)
        {
            if (!rules.Any(rule => rule.IsQualificationRule == true))
            {
                return false;
            }
            else
            {
                foreach (var rule in rules.Where(rule => rule.IsQualificationRule == true))
                {
                    if (!CheckQualificationRule(rule, msg))
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        /// <summary>
        /// Private Method for applying the Regex rules on the value from Mail Message depending on ParsingRule.From property
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="regex"></param>
        /// <param name="msg"></param>
        /// <returns>string</returns>
        private string ApplyCondition(ParsingRuleDTO rule, MailMessage msg)
        {
            switch (rule.EmailField)
            {
                case ("Body"):
                    return GetValue(rule.Regex, msg.Body);

                case ("From"):
                    return GetValue(rule.Regex, msg.From.DisplayName);

                case ("Subject"):
                    return GetValue(rule.Regex, msg.Subject);
                case ("Attachment"):
                    return GetValue(rule.Regex, GetContentFromAttachment(msg));

                default:
                    return string.Empty;
            }

        }

        /// <summary>
        /// Private method to apply the Qualification Rule
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private bool CheckQualificationRule(ParsingRuleDTO rule, MailMessage msg)
        {

            var text = msg.From.DisplayName;
            switch (rule.EmailField)
            {
                case ("Body"):
                    return Regex.IsMatch(msg.Body, rule.Regex);

                case ("From"):
                    return Regex.IsMatch(text, rule.Regex);

                case ("Subject"):
                    return Regex.IsMatch(msg.Subject, rule.Regex);

                case ("Attachment"):
                    foreach (var attachment in msg.Attachments)
                    {
                        var check = Regex.IsMatch(attachment.Name, rule.Regex);
                        if (check)
                        {
                            _attachment = attachment.Name;
                            return true;
                        }
                    }
                    return false;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Private Method for applying the regex on a string and returning the matching string
        /// </summary>
        /// <param name="regex"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private string GetValue(string regex, string data)
        {
            try
            {
               
                return Regex.Match(data, regex).Groups[0].Value.Trim();
            }
            catch (RegexMatchTimeoutException)
            {
                throw new RegexMatchTimeoutException(string.Format("Regex {0} timed out", regex));
            }
        }

        /// <summary>
        /// Private method for getting the content of the attachment found in MailMessage Object. If there was Qualification rule for the Attachment
        /// we are using the attachment that passed the Qualification rule. If not, we are tryign to use the first one in the Mail Message list of attachments.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private string GetContentFromAttachment(MailMessage msg)
        {
            var attachments = _client.GetAttachments(msg);
            if (!attachments.Any())
            {
                return string.Empty;
            }
            else
            {
                if (_attachment != string.Empty)
                {
                    var parser = new AttachmentHandler(attachments.Where(x => x.Contains(_attachment)).SingleOrDefault());
                    return parser.ReadFile();
                }
                else
                {
                    var parser = new AttachmentHandler(attachments.SingleOrDefault());
                    return parser.ReadFile();
                }
            }
        }


    }


}


