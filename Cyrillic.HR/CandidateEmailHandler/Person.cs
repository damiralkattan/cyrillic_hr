﻿namespace CandidateEmailHandler
{
    public class Person
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string JobApplied { get; set; }
    }
}
