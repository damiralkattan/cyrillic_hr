﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace CandidateEmailHandler.Tests
{
    [TestClass]
    public class CandidateEmailHandlerTests
    {

        [TestMethod]
        public void TestConnect()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);

            var handler = new EmailClient();

            Assert.IsTrue(handler.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute));
        }

        [TestMethod]
        public void TestConnectFail()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            var handler = new EmailClient();
            try
            {
                handler.Connect(Constants.EmailAdress, "Wrong password", Constants.EmailHost, 993, attachmentFolderAbsolute);
                Assert.Fail();
            }
            catch (S22.Imap.InvalidCredentialsException)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void TestGetMessages()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);
            var msgs = client.GetMessages("Inbox");

            foreach (var msg in msgs)
            {
                if (msg.Body.Equals(null) || msg.From.Equals(null) || msg.Subject.Equals(null))
                {
                    Assert.Fail();
                }
            }

            Assert.IsTrue(msgs.Count() != 0);
        }

        [TestMethod]
        public void TestGetMessagesFail()
        {

            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);
            try
            {
                var msgs = client.GetMessages("Wrong Inbox");
                Assert.Fail();
            }
            catch (S22.Imap.BadServerResponseException)
            {
                Assert.IsTrue(true);
            }

            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestGetAttachments()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            EmptyFolder(attachmentFolderAbsolute);


            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);
            var msgs = client.GetMessages("C#_Infostud");
            var attachments = client.GetAttachments(msgs.FirstOrDefault());


            Assert.IsTrue(attachments.Count != 0);
        }

        [TestMethod]
        public void TestGetMessagesEmptyInbox()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            EmptyFolder(attachmentFolderAbsolute);


            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);

            var msgs = client.GetMessages("Drafts");
            Assert.IsFalse(msgs.Count() != 0);

        }

        [TestMethod]
        public void TestGetCandidatesFullName()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);
            var parser = new EmailParser(client, "Inbox");
            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "From",
                Regex = @"\bindeedemail.com\b"
            };
            var parsingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "From",
                Regex = "\".*?\"",
                CandidateProperty = "FullName"
            };
            var list = new List<ParsingRuleDTO>() { qualifyingRule, parsingRule };
            var persons = parser.GetCandidates(list);

            Assert.IsTrue(persons.Any(x => x.FullName == "\"Jessie Malinowski\""));
        }

        [TestMethod]
        public void TestGetCandidatesWrongQualifyingRule()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);
            var parser = new EmailParser(client, "Inbox");
            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "From",
                Regex = @"Wrong Regex"
            };
            var parsingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "From",
                Regex = "\".*?\"",
                CandidateProperty = "FullName"
            };
            var list = new List<ParsingRuleDTO>() { qualifyingRule, parsingRule };
            var persons = parser.GetCandidates(list);

            Assert.IsTrue(!persons.Any());
        }
        [TestMethod]
        public void TestGetCandidatesWrongParsingRule()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);
            var parser = new EmailParser(client, "Inbox");
            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "From",
                Regex = @"\bindeedemail.com\b"
            };
            var parsingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "From",
                Regex = "WrongRegex",
                CandidateProperty = "FullName"
            };
            var list = new List<ParsingRuleDTO>() { qualifyingRule, parsingRule };
            var persons = parser.GetCandidates(list);

            Assert.IsTrue(!persons.Any(x=>x.FullName != string.Empty));
        }
        [TestMethod]
        public void TestGetCandidatesEmail()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);
            var parser = new EmailParser(client, "Inbox");
            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "From",
                Regex = @"\bposlovi.infostud.com\b"
            };
            var parsingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "Body",
                Regex = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                CandidateProperty = "Email"
            };
            var list = new List<ParsingRuleDTO>() { qualifyingRule, parsingRule };
            var persons = parser.GetCandidates(list);

            Assert.IsTrue(persons.Any(x => x.Email == "mladen.cukovic87@gmail.com"));
        }

        [TestMethod]
        public void TestGetCandidatesEmailFromAttachmentIndeed()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);

            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);

            var parser = new EmailParser(client, "Inbox");

            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "From",
                Regex = @"\bindeedemail.com\b"
            };
            var parsingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "Attachment",
                Regex = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                CandidateProperty = "Email"
            };

            var list = new List<ParsingRuleDTO>() { qualifyingRule, parsingRule };
            var persons = parser.GetCandidates(list);

            Assert.IsTrue(persons.Any(x => x.Email.Equals("williamsurber5_h5a@indeedemail.com")));
        }

        [TestMethod]
        public void TestGetCandidatesEmailFromAttachmentInfoStud()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);

            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);

            var parser = new EmailParser(client, "Inbox");
            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "Body",
                Regex = @"\bposlovi.infostud.com\b"
            };
            var qualifyingRuleAttachment = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "Attachment",
                Regex = @"\bCV\b"
            };
            var parsingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "Attachment",
                Regex = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                CandidateProperty = "Email"
            };

            var list = new List<ParsingRuleDTO>() { qualifyingRule, qualifyingRuleAttachment, parsingRule };
            var persons = parser.GetCandidates(list);

            Assert.IsTrue(persons.Any(x => x.Email.Equals("stefanzoran@gmail.com")));
        }

        [TestMethod]
        public void TestGetCandidateApplyRules()
        {

            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);
            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);
            var parser = new EmailParser(client, "Inbox");
            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "From",
                Regex = @"\bposlovi.infostud.com\b"
            };
            var parsingRuleEmail = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "Body",
                Regex = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                CandidateProperty = "Email"
            };
            var parsingRuleName = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "From",
                Regex = @"(\w+\s+){2}",
                CandidateProperty = "FullName"
            };

            var list = new List<ParsingRuleDTO>() { qualifyingRule, parsingRuleEmail, parsingRuleName };
            var persons = parser.GetCandidates(list);
            Assert.IsTrue(persons.Any(x => x.Email == "mladen.cukovic87@gmail.com") && persons.Any(x=>x.FullName == "Mladen Ćuković"));
        }

        [TestMethod]
        public void TestGetJobAppliedToInfostud()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);

            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);

            var parser = new EmailParser(client, "PHP_Infostud");
            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "Body",
                Regex = @"\bposlovi.infostud.com\b"
            };
            var parsingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "Subject",
                Regex = @"(?<=\-).*",
              
                CandidateProperty = "JobApplied"
            };

            var list = new List<ParsingRuleDTO>() { qualifyingRule, parsingRule };
            var persons = parser.GetCandidates(list);

            Assert.IsTrue(persons.Any(x => x.JobApplied.Equals("PHP - Drupal Developer")));
        }

        [TestMethod]
        public void TestGetCandidatesJobAppliedToIndeed()
        {
            var executionFolder = AppDomain.CurrentDomain.BaseDirectory;
            var attachmentsFolderRelative = @"..\..\Attachments";
            var attachmentFolderAbsolute = Path.Combine(executionFolder, attachmentsFolderRelative);

            var client = new EmailClient();
            client.Connect(Constants.EmailAdress, Constants.EmailPassword, Constants.EmailHost, 993, attachmentFolderAbsolute);

            var parser = new EmailParser(client, "Inbox");

            var qualifyingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = true,
                EmailField = "From",
                Regex = @"\bindeedemail.com\b"
            };
            var parsingRule = new ParsingRuleDTO()
            {
                IsQualificationRule = false,
                EmailField = "Subject",
                Regex = @"^.*?(?=candidate)",
                CandidateProperty = "JobApplied"
            };

            var list = new List<ParsingRuleDTO>() { qualifyingRule, parsingRule };
            var persons = parser.GetCandidates(list);

            Assert.IsTrue(persons.Any(x => x.JobApplied.Equals("Executive Assistant/Marketing Coordinator")));
        }

        [TestMethod]
        public void TestSkillsParser()
        {
            var parser = new SkillsParser();

            var checkSkills = new List<string>()
            {
                "C#",
                "C++",
                "Java",
                "Agile"
            };
            var skills = parser.ReadSkills(@"C:\Users\Damir\Downloads\example.xlsx","Skills Matrix", checkSkills);

            Assert.IsTrue(skills[0].SkillLevel.Equals("Junior") && skills[1].SkillLevel.Equals("Mid") && skills[2].SkillLevel.Equals("Mid") && skills[3].SkillLevel.Equals("Junior"));
        }

        [TestMethod]
        public void TestSkillsParserNonExistentSkill()
        {
            var parser = new SkillsParser();

            var checkSkills = new List<string>()
            {
                "Skill Doesnt Exist"
               
            };
            var skills = parser.ReadSkills(@"C:\Users\Damir\Downloads\example.xlsx","Skills Matrix", checkSkills);

            Assert.IsTrue(skills.Count == 0);
        }

        [TestMethod]
        public void TestSkillsParserReadAll()
        {
            var parser = new SkillsParser();

           
            var skills = parser.ReadAllSkills(@"C:\Users\Damir\Downloads\example.xlsx", "Skills Matrix");

            Assert.IsTrue(true);
        }


        private void EmptyFolder(string path)
        {
            var di = new DirectoryInfo(path);

            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (var dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }



    }
}
